
#********************************** VARIABLES *********************************#

# directory vars
ROOT_DIR = ./
SRC_DIR = src
OBJ_DIR = obj
BIN_DIR = bin
LIB_DIR = lib
ETC_DIR = etc
DATA_DIR = data
OUTPUT_DIR = output

# target vars
MAIN_PY = main_python_script
CUDA_GRAD_DESC_EXE = $(BIN_DIR)/cuda_grad_desc

# src files
CUDA_GRAD_DESC_SRC =	$(OBJ_DIR)/cuda_grad_desc.o
CUDA_GRAD_DESC_SRC +=	$(OBJ_DIR)/data.o
CUDA_GRAD_DESC_SRC +=	$(OBJ_DIR)/gradient_descent.o

TARGETS =	$(CUDA_GRAD_DESC_EXE)
# TARGETS +=	$(MAIN_PY)

PROCESS_TYPE = gd0

# TRAINING_DATAFILE = $(DATA_DIR)/users_to_movies/all.dta
# TRAINING_DATA_INDEXFILE = $(DATA_DIR)/users_to_movies/all.idx
# QUAL_DATAFILE = $(DATA_DIR)/users_to_movies/qual.dta
# OUTPUT_DATAFILE = $(OUTPUT_DIR)/users_to_movies/out.dta

# the default submission order is movies to users
TRAINING_DATAFILE = $(DATA_DIR)/movies_to_users/all.dta
TRAINING_DATA_INDEXFILE = $(DATA_DIR)/movies_to_users/all.idx
QUAL_DATAFILE = $(DATA_DIR)/movies_to_users/qual.dta
OUTPUT_DATAFILE = $(OUTPUT_DIR)/movies_to_users/out_$(PROCESS_TYPE).dta


#************************************ CPP *************************************#

CXX			= g++-5
CXXFLAGS	= -std=c++14 -g -Wall -pedantic -O3

INCLUDE		= -I$(LIB_DIR) -I/usr/lib/include
LD_LIBDIR	= -L/usr/local/lib
LD_LIBS		= #

#************************************ CUDA ************************************#

CUDA_PATH		= /usr/local/cuda
CUDA_INC_PATH	= $(CUDA_PATH)/include
CUDA_BIN_PATH	= $(CUDA_PATH)/bin
CUDA_LIB_PATH	= $(CUDA_PATH)/lib64

NVCC = $(CUDA_BIN_PATH)/nvcc
NVCCFLAGS = -std=c++11 -m64 -O3

GENCODE_FLAGS = -gencode arch=compute_30,code=sm_30

INCLUDE		+= -I$(CUDA_INC_PATH)
LD_LIBDIR	+= -L$(CUDA_LIB_PATH)
LD_LIBS		+= -lcudart # -cufft

#*********************************** PYTHON ***********************************#

# binary variables
PYTHON		= python


#************************************ RULES ***********************************#

# generic rules
all: $(TARGETS)

clean:
	rm -f $(OBJ_DIR)/*.o $(TARGETS)

again: clean all

.PHONY: all clean


# project build/run rules
$(MAIN_PY):
	$(PYTHON) $(SRC_DIR)/main.py -t$(PROCESS_TYPE) \
		$(TRAINING_DATAFILE) $(TRAINING_DATA_INDEXFILE) $(QUAL_DATAFILE) \
		$(OUTPUT_DATAFILE)

$(CUDA_GRAD_DESC_EXE): $(CUDA_GRAD_DESC_SRC)
	$(CXX) $(CXXFLAGS) $(INCLUDE) $^ -o $@ $(LD_LIBDIR) $(LD_LIBS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@ $(LD_LIBS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cu
	$(NVCC) $(NVCCFLAGS) $(INCLUDE) $(GENCODE_FLAGS) -c $^ -o $@

#************************************ END *************************************#
