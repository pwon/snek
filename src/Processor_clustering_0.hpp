// Header function for main clustering processing function.
#pragma once

#include "common.hpp"
#include "data.hpp"
#include "similarity.hpp"
#include "neighborhood.hpp"
#include <string>
#include <utility>
#include <random>
#include <chrono>
#include <time.h>

// Main function which leads in data from the cache, trains a clustering
// model, and then makes predictions on the qual set and writes them out to a
// file.
int process();

