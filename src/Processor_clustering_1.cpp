// Trains and then makes predictions using a clustering model.

#include "Processor_clustering_0.hpp"

#define NITEMS 17770
#define NUSERS 458293

int process()
{
	// Load the data in the Data namespace.
	Data::init();
	// Data::cache();
	Data::load();

	// The approximate mean movie rating before 1500 days (mean1) and after
	// 1500 days (mean2), hardcoded for convenience.
	float mean1 = 3.40583;
	float mean2 = 3.64912;

	// Hyperparameter K which is max number of neighbors we'll consider.
	int K = 20;
	// Hyperparameter for shrinkage of similarities by support.
	// float alpha = 500.0;
	// Hyperparameters for the clustering algorithms.
	// float gamma = -2.534;
	// float delta = 11.4;
	// float beta = 500.0;


	// Load in our precomputed similarity matrix.
	// std::cout << "Loading similarity matrix S..." << std::endl;
	// std::vector<float> S;
	// std::ifstream binary_in_data_file;
	// // binary_in_data_file.open("../cache/movies_to_users/corrcoef.bch", std::os::in | std::os:binary);
	// binary_in_data_file.open("../cache/movies_to_users/stest.bch", std::ios::in | std::ios::binary);
	// binary_in_data_file.read((char *) S.data(), NITEMS * NITEMS * sizeof(float));
	// binary_in_data_file.close();
	// std::cout << "DONE!" << std::endl;

	// Load in our precomputed pairwise similarity matrix.
	// printf("Loading pairwise support matrix U...");
	// std::vector<int> U;
	// std::ifstream binary_U_data_file;
	// binary_U_data_file.open("../cache/movies_to_users/pwisesupp.bch", std::ios::in | std::ios::binary);
	// binary_U_data_file.read((char *) U.data(), NITEMS * NITEMS * sizeof(int));
	// binary_U_data_file.close();
	// printf("DONE!\n");

	// Shrink the similarities by their supports using hyperparameter alpha.
	// printf("Calculating shrinked similarities...");
	// std::vector<float> S_reg(S);
	// for (int i = 0; i < S_reg.size(); i++)
	// {
	// 	S_reg[i] *= U[i] / (U[i] + alpha);
	// }
	// printf("DONE!\n");

	// Mean-centers the training data; we'll train the data on the residuals
	// after mean-centering. We can also add other global effects processing
	// at this stage.
	std::cout << "Processing training data..." << std::endl;
	std::unordered_map<std::string, float> ratings;
	std::unordered_map<int, std::vector<int>> itemmap;
	// std::unordered_map<std::string, float> times;
	// Try to prevent rehashing by reserving a lot of space at once.
	ratings.reserve(94362233);
	// times.reserve(94362233);
	itemmap.reserve(94262233);
	for (int i = 0; i < Data::train.size(); i++)
	{
		Data::Entry entry = Data::train[i];
		float residual = 0.0;
		if (entry.date_id <= 1500)
		{
			residual = ((float) entry.rating) - mean1;
		}
		else
		{
			residual = ((float) entry.rating) - mean2;
		}
		if (itemmap.find(entry.movie_id) != itemmap.end())
		{
			itemmap[entry.movie_id].push_back(entry.user_id);
		}
		else
		{
			std::vector<int> t (1, entry.user_id);
			itemmap[entry.movie_id] = t;
		}
		ratings[std::to_string(entry.user_id) + "," + std::to_string(entry.movie_id)] = residual;
		// times[std::to_string(entry.user_id) + "," + std::to_string(entry.movie_id)] = entry.date_id;
		if (i % 1000000 == 0)
		{
			std::cout << "Processed " << i << " data points!" << std::endl;
		}
	}
	std::cout << "DONE!" << std::endl;

	// We don't need to train our neighborhood model.

	// Calculate the RMSE of our initial parameter settings.
	// std::cout << "Calculating RMSE..." << std::endl;
	// float best_RMSE = 0.0;
	// for (int i = 0; i < Data::test.size(); i++)
	// {
	// 	Data::Entry entry = Data::test[i];
	// 	// float predicted = kNNBasic(entry, S_reg, ratings, K);
	// 	float predicted = kNNBasic(entry, S, ratings, K);
	// 	if (entry.date_id <= 1500)
	// 	{
	// 		predicted += mean1;
	// 	}
	// 	else
	// 	{
	// 		predicted += mean2;
	// 	}
	// 	best_RMSE += (entry.rating - predicted) * (entry.rating - predicted);
	// }
	// std::cout << "Initial (squared) RMSE on training set is " << best_RMSE << std::endl;


	// Tune our parameters using our test set. For right now just greedily
	// tune them: that is, find the best value for our first hyperparameter
	// alpha, then given that best alpha, find delta, gamma, etc.

	// int nepochs = 10; // Number of times to run paramter tuning.
	// int nparams = 4; // Number of parameters to tune in total.

	// float best_alpha = alpha;
	// float best_beta = beta;
	// float best_delta = delta;
	// float best_gamma = gamma;
	// printf("Tuning %d hypereparameters for %d epochs...\n", nparams, nepochs);
	// for (int i = 0; i < nepochs; i++)
	// {
	// 	printf("Epoch %d----------\n", i);
	// 	printf("Current alpha is %f\n", best_alpha);
	// 	printf("Current delta is %f\n", best_delta);
	// 	printf("Current gamma is %f\n", best_gamma);
	// 	printf("Current beta is %f\n", best_beta);
	// 	srand(time(NULL));
	// 	int rand_param = rand() % nparams + 1;
	// 	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	// 	std::default_random_engine generator(seed);
	// 	// Sample from a normal distribution whose mean is the best value of
	// 	// our parameter so far and whose std. dev. is 0.1 times the mean.
	// 	std::normal_distribution<float> dist(best_param, 0.1 * abs(best_param));
	// 	float new_param = dist(generator);
	// 	// If we picked alpha, we need to recalculate the shrinked similarities.
	// 	if (rand_param == 1)
	// 	{
	// 		for (int j = 0; j < S_reg.size(); j++)
	// 		{
	// 			S_reg = (S[j] * U[j]) / (U[j] + new_param);
	// 		}
	// 	}
	// 	// Calculate the RMSE on the test set.
	// 	printf("Chose param %d\n", rand_param);
	// 	printf("Value to test of new param is %f\n", new_param);
	// 	printf("Calculating RMSE...");
	// 	float RMSE = 0.0;
	// 	for (int i = 0; i < Data::test.size(); i++)
	// 	{
	// 		Data::Entry entry = Data::test[i];
	// 		float predicted = 0.0;
	// 		// Predicted residual from our model.
	// 		switch (rand_param)
	// 		{
	// 			case 1:
	// 				predicted = kNNBasic(entry, S_reg, ratings, K);
	// 			case 2:
	// 				predicted = kNNBasic(entry, S_reg, ratings, K);
	// 			case 3:
	// 				predicted = kNNBasic(entry, S_reg, ratings, K);
	// 			case 4:
	// 				predicted = kNNBasic(entry, S_reg, ratings, K);

	// 		}
	// 		// Undo global effects
	// 		if (entry.date_id <= 1500)
	// 		{
	// 			predicted += mean1;
	// 		}
	// 		else
	// 		{
	// 			predicted += mean2;
	// 		}
	// 		RMSE += (entry.rating - predicted) * (entry.rating - predicted);
	// 	}
	// 	printf("%f\n", RMSE);
	// 	// Technically squared RMSE.
	// 	if (RMSE < best_RMSE)
	// 	{
	// 		best_RMSE = RMSE;
	// 		switch (rand_param)
	// 		{
	// 			case 1:
	// 				best_alpha = new_param;
	// 			case 2:
	// 				best_delta = new_param;
	// 			case 3:
	// 				best_gamma = new_param;
	// 			case 4:
	// 				best_beta = new_param;
	// 		}
	// 	}
	// }
	// printf("Finished tuning parameters!\n");
	// printf("Best alpha is %f\n", best_alpha);
	// printf("Best delta is %f\n", best_delta);
	// printf("Best gamma is %f\n", best_gamma);
	// printf("Best beta is %f\n", best_beta);
	// printf("Best RMSE is %f\n", best_RMSE);

	// Make predictions on the qual set.
	std::cout << "Predicting qual set ratings..." << std::endl;
	std::ofstream output_file;
	output_file.open("output/movies_to_users/clustering_test_K20.dta", std::ios::out);
	std::for_each(Data::qual.begin(), Data::qual.end(), [&](const Data::Entry &entry)
	{
		// Get the predicted residual from our clustering model.
		// float predicted = kNNBasic(entry, S_reg, ratings, K);
		// float predicted = kNNBasic(entry, S, ratings, K);
		std::vector<float> sims(NITEMS);
		std::vector<int> users_rating_i = itemmap[entry.movie_id];
		std::sort(users_rating_i.begin(), users_rating_i.end());
		std::vector<std::pair<int, float>> vec1;
		for (int i = 0; i < users_rating_i.size(); i++)
		{
			float rating_i = ratings[std::to_string(users_rating_i[i]) + "," + std::to_string(entry.movie_id)];
			vec1.push_back(std::pair<int, float>(users_rating_i[i] - 1, rating_i));
		}
		for (int i = 1; i < NITEMS + 1; i++)
		{
			std::vector<int> users_rating_j = itemmap[i];
			std::sort(users_rating_j.begin(), users_rating_j.end());
			std::vector<std::pair<int, float>> vec2;
			for (int j = 0; j < users_rating_j.size(); j++)
			{
				float rating_j = ratings[std::to_string(users_rating_j[j]) + "," + std::to_string(i)];
				vec2.push_back(std::pair<int, float>(users_rating_j[j] - 1, rating_j));
			}
			sims[i - 1] = corrCoef2(vec1, vec2);
		}
		std::vector<int> topk = TopK(sims, K);

		float wsum = 0.0;
		float norm = 0.0;

		for (int i = 0; i < K; i++)
		{
			std::string key = std::to_string(entry.user_id) + "," + std::to_string(topk[i]);
			if (ratings.find(key) != ratings.end())
			{
				wsum += sims[topk[i]] * ratings[key];
				norm += sims[topk[i]];
			}
		}
		float predicted = 0.0;
		if (norm != 0)
		{
			predicted = wsum / norm;
		}

		// Undo our mean-centering.
		if (entry.date_id <= 1500)
		{
			predicted += mean1;
		}
		else
		{
			predicted += mean2;
		}

		// Clip the output at 1 and 5 if it is out of bounds.
		if (predicted < 1.0)
		{
			predicted = 1.0;
		}
		else if (predicted > 5.0)
		{
			predicted = 5.0;
		}
		output_file << predicted << "\n";
	});
	output_file.close();
	std::cout << "DONE!\n" << std::endl;

	return 0;
}

int main()
{
	process();
	return 0;
}