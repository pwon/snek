#pragma once

// C libraries
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cassert>
#include <ctime>
#include <climits>
#include <cfloat>

// Streams
#include <iostream>
#include <fstream>

// Containers
#include <vector>
#include <unordered_set>
#include <unordered_map>

// Functional
#include <algorithm>

// Memory
#include <memory>
