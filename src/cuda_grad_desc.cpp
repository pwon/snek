
#include "common.hpp"
#include "common_cuda.hpp"

#include "data.hpp"
#include "gradient_descent.cuh"

int main(int argc, char const *argv[])
{
    Data::init();
    // Data::cache();                   // <- Run only once!
    Data::load();

    // Data::batchTrainData();          // <- Run only once!
    // Data::cacheTrainDataBatches();   // <- Run only once!
    Data::loadCachedTrainDataBatches();

    Data::computeMetadata();            // <- Run only once!
    // Data::cacheMetadata();              // <- Run only once!
    // Data::loadCachedMetadata();

    GradientDescent::PredictionModel model;

    assert(argc == 5);
    model.init(atoi(argv[1]), atof(argv[2]), atof(argv[3]), atof(argv[4]));
    model.cudaLearnBatches();
    printf("\nDONE LEARNING!\n");

    const char *output_filename =
        "output/movies_to_users/cuda_gd_reg_out_new_512_0001_001.dta";
    printf("\nWriting out qual results to: %s\n", output_filename);
    std::ofstream output_file;
    output_file.open(output_filename, std::ios::out);
    std::for_each(Data::qual.begin(), Data::qual.end(),
        [&](const Data::Entry &entry)
        {
            output_file << model.predictCPU(entry.user_id, entry.movie_id)
                << "\n";
        });
    output_file.close();
    printf("DONE!\n");

    Data::cleanup();

    return 0;
}

// 40/0.001 -> 0.86799237129
