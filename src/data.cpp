#include "data.hpp"

namespace Data
{

/*********************************** GENERIC **********************************/

std::vector<Entry> train(k_train_count);
std::vector<Entry> test(k_test_count);
std::vector<Entry> qual(k_qual_count);

void init()
{
    // TODO: Implement smart caching
}

void cache()
{
    std::ifstream all_data_file("data/movies_to_users/all.dta");
    if (!all_data_file.is_open())
    {
        fprintf(stderr, "ERROR: Could not open file: %s\n",
            "data/movies_to_users/all.dta");
        exit(1);
    }

    std::ifstream all_index_file("data/movies_to_users/all.idx");
    if (!all_data_file.is_open())
    {
        fprintf(stderr, "ERROR: Could not open file: %s\n",
            "data/movies_to_users/all.idx");
        exit(1);
    }

    int all_cursor = 0;
    int train_cursor = 0;
    int test_cursor = 0;
    int qual_cursor = 0;

    const int k_read_buffer_size = 256;
    std::array<char, k_read_buffer_size> data_read_buffer;
    std::array<char, k_read_buffer_size> index_read_buffer;
    while (all_data_file.getline(data_read_buffer.data(), k_read_buffer_size))
    {
        assert(all_index_file.getline(index_read_buffer.data(),
            k_read_buffer_size));
        assert(all_cursor < k_all_count);

        int data_type = atoi(strtok(index_read_buffer.data(), " "));
        assert(strtok(NULL, " ") == NULL);
        switch (data_type)
        {
            case 1:
                train[train_cursor].user_id =
                    atoi(strtok(data_read_buffer.data(), " "));
                train[train_cursor].movie_id = atoi(strtok(NULL, " "));
                train[train_cursor].date_id = atoi(strtok(NULL, " "));
                train[train_cursor].rating = atoi(strtok(NULL, " "));
                train_cursor++;
                break;
            case 2:
            case 3:
            case 4:
                test[test_cursor].user_id =
                    atoi(strtok(data_read_buffer.data(), " "));
                test[test_cursor].movie_id = atoi(strtok(NULL, " "));
                test[test_cursor].date_id = atoi(strtok(NULL, " "));
                test[test_cursor].rating = atoi(strtok(NULL, " "));
                test_cursor++;
                break;
            case 5:
                qual[qual_cursor].user_id =
                    atoi(strtok(data_read_buffer.data(), " "));
                qual[qual_cursor].movie_id = atoi(strtok(NULL, " "));
                qual[qual_cursor].date_id = atoi(strtok(NULL, " "));
                qual[qual_cursor].rating = atoi(strtok(NULL, " "));
                qual_cursor++;
                break;
        }
        assert(strtok(NULL, " ") == NULL);

        all_cursor++;

        if (all_cursor % 1000000 == 0)
        {
            printf("Loaded %d entries into pre-cache buffer\n",
                all_cursor);
        }
    }
    assert(all_cursor == k_all_count);
    assert(train_cursor == k_train_count);
    assert(test_cursor == k_test_count);
    assert(qual_cursor == k_qual_count);
    assert(train_cursor == (int) train.size());
    assert(test_cursor == (int) test.size());
    assert(qual_cursor == (int) qual.size());
    assert(train_cursor == (int) train.capacity());
    assert(test_cursor == (int) test.capacity());
    assert(qual_cursor == (int) qual.capacity());
    printf("DONE! Loaded %d entries into pre-cache buffer\n", all_cursor);

    std::ofstream binary_out_data_file;

    binary_out_data_file.open("cache/movies_to_users/train.dta.bch",
        std::ios::out | std::ios::binary);
    binary_out_data_file.write((char*) train.data(),
        k_train_count * sizeof (Entry));
    binary_out_data_file.close();

    binary_out_data_file.open("cache/movies_to_users/test.dta.bch",
        std::ios::out | std::ios::binary);
    binary_out_data_file.write((char*) test.data(),
        k_test_count * sizeof (Entry));
    binary_out_data_file.close();

    binary_out_data_file.open("cache/movies_to_users/qual.dta.bch",
        std::ios::out | std::ios::binary);
    binary_out_data_file.write((char*) qual.data(),
        k_qual_count * sizeof (Entry));
    binary_out_data_file.close();

    printf("DONE! Finished caching!\n");
}

void load()
{
    printf("Loading from cache!\n");
    std::ifstream binary_in_data_file;

    binary_in_data_file.open("cache/movies_to_users/train_old.dta.bch",
        std::ios::in | std::ios::binary);
    // binary_in_data_file.open("cache/movies_to_users/train.dta.bch",
    //     std::ios::in | std::ios::binary);
    binary_in_data_file.read((char*) train.data(),
        k_train_count * sizeof (Entry));
    binary_in_data_file.close();

    binary_in_data_file.open("cache/movies_to_users/test_old.dta.bch",
        std::ios::in | std::ios::binary);
    // binary_in_data_file.open("cache/movies_to_users/test.dta.bch",
    //     std::ios::in | std::ios::binary);
    binary_in_data_file.read((char*) test.data(),
        k_test_count * sizeof (Entry));
    binary_in_data_file.close();

    binary_in_data_file.open("cache/movies_to_users/qual_old.dta.bch",
        std::ios::in | std::ios::binary);
    // binary_in_data_file.open("cache/movies_to_users/qual.dta.bch",
    //     std::ios::in | std::ios::binary);
    binary_in_data_file.read((char*) qual.data(),
        k_qual_count * sizeof (Entry));
    binary_in_data_file.close();
    printf("DONE!\n");
}

void cleanup()
{
    // TODO: Any final caching left to do
}


/************************************ INDEX ***********************************/

// Index train data for faster batching
static std::vector<int> indexed_train_data_indices(k_train_count);
void indexTrainData()
{
    printf("GROUP TRAINING DATA BY MOVIE ID\n");

    std::vector<std::vector<int>> movie_groups(k_movie_count);
    int buffer_capacity = k_train_count / k_movie_count;
    for (auto &group : movie_groups)
    {
        group.reserve(buffer_capacity);
    }

    for (int i = 0; i < k_train_count; i++)
    {
        movie_groups.at(train.at(i).movie_id - 1).push_back(i);
    }

    int index_id = 0;
    int start_offset = 0;
    bool delete_required = false;

    while (!movie_groups.empty())
    {
        auto group_it = movie_groups.begin() + start_offset;
        for (; group_it < movie_groups.end(); group_it++)
        {
            indexed_train_data_indices.at(index_id) = group_it->back();
            group_it->pop_back();
            index_id++;
            if (group_it->empty())
            {
                start_offset = group_it - movie_groups.begin();
                delete_required = true;
                break;
            }
        }

        if (delete_required)
        {
            movie_groups.erase(group_it);
            delete_required = false;
        }
        else
        {
            start_offset = 0;
        }
    }

    printf("DONE\n");
}


/************************************ BATCH ***********************************/

std::vector<Batch> batches;
static std::unordered_set<std::unique_ptr<BatchMetaData>> available_batches;

BatchMetaData::BatchMetaData(int index)
    : index(index)
    , entry_count()
    , users()
    , movies()
{
    //
}

bool BatchMetaData::full() const
{
    assert(this->entry_count > -1 && this->entry_count <= k_batch_size);
    return this->entry_count == k_batch_size;
}

int BatchMetaData::getEntryCount() const
{
    return this->entry_count;
}

bool BatchMetaData::check(const Entry &entry) const
{
    return this->users.find(entry.user_id) == this->users.end()
        && this->movies.find(entry.movie_id) == this->movies.end();
}

void BatchMetaData::add(const Entry &entry)
{
    assert(!this->full());
    assert(this->check(entry));
    batches.at(this->index).at(this->entry_count++) = entry;
    this->users.insert(entry.user_id);
    this->movies.insert(entry.movie_id);
}

void batchTrainData()
{
    indexTrainData();

    printf("START BATCHING!\n");
    batches.reserve(k_train_count / k_batch_size + 1);

    int lost_slot_count = 0;

    int progress_counter = 0;
    for (const int index : indexed_train_data_indices)
    {
        const Entry &entry = train.at(index);

        if (available_batches.empty())
        {
            batches.emplace_back();
            available_batches.insert(
                std::make_unique<BatchMetaData>(batches.size() - 1));
        }

        auto batch_meta_it = available_batches.begin();
        for (; batch_meta_it != available_batches.end(); batch_meta_it++)
        {
            if ((*batch_meta_it)->check(entry))
            {
                (*batch_meta_it)->add(entry);
                break;
            }
        }

        if (batch_meta_it == available_batches.end())
        {
            batches.emplace_back();
            auto new_batch =
                std::make_unique<BatchMetaData>(batches.size() - 1);
            new_batch->add(entry);
            available_batches.insert(std::move(new_batch));
        }
        else if ((*batch_meta_it)->full())
        {
            available_batches.erase(batch_meta_it);
        }

        progress_counter++;
        if (progress_counter % 100000 == 0)
        {
            printf("Batched %d entries into pre-cache buffer\n",
                progress_counter);
            float avg_batch_size =
                k_batch_size * (batches.size() - available_batches.size());
            float avg_not_full_batch_size = 0.0;
            for (const auto &batch_meta_ptr : available_batches)
            {
                avg_not_full_batch_size += batch_meta_ptr->getEntryCount();
            }
            avg_batch_size += avg_not_full_batch_size;
            avg_batch_size -= lost_slot_count;
            avg_batch_size /= batches.size();
            int potential_lost_slots = available_batches.size() * k_batch_size
                - avg_not_full_batch_size;
            avg_not_full_batch_size /= available_batches.size();

            printf("Total Batch Count: %ld\n", batches.size());
            printf("Left over available_batch count: %ld\n", available_batches.size());
            printf("Average Batch Size: %f\n", avg_batch_size);
            printf("Average Not Full Batch Size: %f\n", avg_not_full_batch_size);

            if (available_batches.size() > 5000)
            {
                lost_slot_count += potential_lost_slots;
                available_batches.clear();
            }
        }
    }

    float avg_batch_size =
        k_batch_size * (batches.size() - available_batches.size());
    float avg_not_full_batch_size = 0.0;
    for (const auto &batch_meta_ptr : available_batches)
    {
        avg_not_full_batch_size += batch_meta_ptr->getEntryCount();
    }
    avg_batch_size += avg_not_full_batch_size;
    avg_batch_size -= lost_slot_count;
    avg_batch_size /= batches.size();
    avg_not_full_batch_size /= available_batches.size();

    printf("DONE!\n");
    printf("Processed: %d entries!\n", progress_counter);
    printf("Total Batch Count: %ld\n", batches.size());
    printf("Left over available_batch count: %ld\n", available_batches.size());
    printf("Average Batch Size: %f\n", avg_batch_size);
    printf("Average Not Full Batch Size: %f\n", avg_not_full_batch_size);

    available_batches.clear();
}

void cacheTrainDataBatches()
{
    printf("CACHING BATCHES!\n");
    std::ofstream binary_out_batch_data_file;

    binary_out_batch_data_file.open("cache/movies_to_users/batch.dta.bch",
        std::ios::out | std::ios::binary);
    binary_out_batch_data_file.write((char*) batches.data(),
        batches.size() * sizeof (Batch));
    binary_out_batch_data_file.close();
    printf("DONE!\n");
}

void loadCachedTrainDataBatches()
{
    printf("Loading batches from cache\n");

    // const int k_batch_count = 318360;   // Hardcoded for now (512)
    const int k_batch_count = 251782;   // Hardcoded for now (1024)
    // const int k_batch_count = 262275;   // Hardcoded for now (1024 new)

    batches.resize(k_batch_count);

    std::ifstream binary_in_data_file;

    binary_in_data_file.open("cache/movies_to_users/batch_1024.dta.bch",
        std::ios::in | std::ios::binary);
    // binary_in_data_file.open("cache/movies_to_users/batch.dta.bch",
    //     std::ios::in | std::ios::binary);
    binary_in_data_file.read((char*) batches.data(),
        k_batch_count * sizeof (Batch));
    binary_in_data_file.close();

    printf("DONE!\n");

    // const Batch &first_batch = batches.at(0);
    // std::unordered_set<int> users;
    // std::for_each(first_batch.begin(), first_batch.end(),
    //     [&users](const Entry &entry)
    //     {
    //         printf("%d %d %d %d\n",
    //             entry.user_id,
    //             entry.movie_id,
    //             entry.date_id,
    //             entry.rating);
    //         assert(users.find(entry.user_id) == users.end());
    //         users.insert(entry.user_id);
    //     });
    // const Batch &last_batch = *(batches.end() - 1);
    // std::unordered_set<int> users;
    // std::for_each(last_batch.begin(), last_batch.end(),
    //     [&users](const Entry &entry)
    //     {
    //         printf("%d %d %d %d\n",
    //             entry.user_id,
    //             entry.movie_id,
    //             entry.date_id,
    //             entry.rating);
    //         assert(users.find(entry.user_id) == users.end() || entry.user_id == 0);
    //         users.insert(entry.user_id);
    //     });
    int batched_entry_count = 0;
    for (const auto &batch : batches)
    {
        for (const auto &entry : batch)
        {
            if (entry.user_id != 0)
            {
                assert(entry.movie_id != 0);
                assert(entry.date_id != 0);
                assert(entry.rating != 0);
                batched_entry_count++;
            }
        }
    }
    printf("batched: %d\n", batched_entry_count);
}


/********************************** METADATA **********************************/

std::vector<int> movie_entry_counts(k_movie_count);
std::vector<int> user_entry_counts(k_user_count);

float overall_rating_avg = 0.0;

void computeMetadata()
{
    std::for_each(train.begin(), train.end(),
        [](const Entry &entry)
        {
            overall_rating_avg += entry.rating;
        });
    overall_rating_avg /= k_train_count;

    auto set_zero_i = [](int &val){ return 0; };
    std::transform(movie_entry_counts.begin(), movie_entry_counts.end(),
        movie_entry_counts.begin(), set_zero_i);
    std::transform(user_entry_counts.begin(), user_entry_counts.end(),
        user_entry_counts.begin(), set_zero_i);

    std::for_each(train.begin(), train.end(),
        [](const Entry &entry)
        {
            movie_entry_counts[entry.movie_id]++;
            user_entry_counts[entry.user_id]++;
        });
}

void cacheMetadata()
{
    printf("Caching metadata...\n");
    std::ofstream binary_out_data_file;

    binary_out_data_file.open("cache/metadata.bch",
        std::ios::out | std::ios::binary);
    binary_out_data_file.write((char*) movie_entry_counts.data(),
        k_movie_count * sizeof (int));
    binary_out_data_file.write((char*) user_entry_counts.data(),
        k_user_count * sizeof (int));
    binary_out_data_file.write((char*) &overall_rating_avg, sizeof (float));

    printf("DONE! Finished caching!\n");
}

void loadCachedMetadata()
{
    printf("Loading metadata from cache!\n");
    std::ifstream binary_in_data_file;

    binary_in_data_file.open("cache/metadata.bch",
        std::ios::in | std::ios::binary);
    binary_in_data_file.read((char*) movie_entry_counts.data(),
        k_movie_count * sizeof (int));
    binary_in_data_file.read((char*) user_entry_counts.data(),
        k_user_count * sizeof (int));
    binary_in_data_file.read((char*) &overall_rating_avg, sizeof (float));
    binary_in_data_file.close();
    printf("DONE! Finished loading!\n");
}


} // END namespace Data
