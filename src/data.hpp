#pragma once

#include "common.hpp"

namespace Data
{
    // Generic management features
    static constexpr int k_all_count = 102416306;
    static constexpr int k_train_count = 94362233;   // old
    static constexpr int k_test_count = 5304175;     // old
    // static constexpr int k_train_count = 98291669;
    // static constexpr int k_test_count = 1374739;
    static constexpr int k_qual_count = 2749898;

    static constexpr int k_movie_count = 17770 + 1; // + 1 for 1-indexing
    static constexpr int k_user_count = 458293 + 1; // + 1 for 1-indexing

    struct Entry
    {
        int user_id;
        int movie_id;
        int date_id;
        int rating;
    };

    extern std::vector<Entry> train;
    extern std::vector<Entry> test;
    extern std::vector<Entry> qual;

    void init();
    void cache();
    void load();
    void cleanup();

    // Batch management features
    // static constexpr int k_batch_size = 512;
    static constexpr int k_batch_size = 1024;
    // ^ 256 is probably the best?
    // source: http://stackoverflow.com/questions/13078301/gtx-680-keplers-and-maximum-registers-per-thread
    typedef std::array<Entry, k_batch_size> Batch;
    class BatchMetaData
    {
    private:
        int index;
        int entry_count;

        std::unordered_set<int> users;
        std::unordered_set<int> movies;

        // Disable default constructor (must have an ID)
        BatchMetaData();

    public:
        BatchMetaData(int index);
        ~BatchMetaData() = default;

        bool full() const;
        int getEntryCount() const;
        bool check(const Entry &entry) const;
        void add(const Entry &entry);
    };

    extern std::vector<Batch> batches;

    void batchTrainData();
    void cacheTrainDataBatches();
    void loadCachedTrainDataBatches();

    // Metadata features
    extern std::vector<int> movie_entry_counts;
    extern std::vector<int> user_entry_counts;

    extern float overall_rating_avg;

    void computeMetadata();
    void cacheMetadata();
    void loadCachedMetadata();
}
