#include "gradient_descent.cuh"

namespace GradientDescent
{

/****************************** Helper Functions ******************************/

CUDA_CALLABLE inline int _index_mf(int movie_id, int basis_id)
{
    return movie_id + basis_id * Data::k_movie_count;
}

CUDA_CALLABLE inline int _index_uf(int user_id, int basis_id)
{
    return user_id + basis_id * Data::k_user_count;
}

CUDA_CALLABLE inline unsigned int _geq_pow_of_2(int num)
{
    assert(num > 0);
    static const unsigned int k_first_digit_mask = 1;
    unsigned int result = 1;
    int num_one_digits = 0;
    while (num > 0)
    {
        if (num & k_first_digit_mask == k_first_digit_mask)
        {
            num_one_digits++;
        }
        num >>= 1;
        result <<= 1;
    }
    if (num_one_digits == 1)
    {
        result >>= 1;
    }
    return result;
}


/******************************* PredictionModel ******************************/

PredictionModel::PredictionModel()
    : feature_vec_dim(0)
    , movie_features()
    , user_features()
    , movie_features_buffer()
    , user_features_buffer()
    , learning_rate()
    , regularization_factor()
    , initialized(false)
    , gpu_self(nullptr)
    , gpu_movie_features(nullptr)
    , gpu_user_features(nullptr)
    , gpu_test_data(nullptr)
    , gpu_rmse(nullptr)
    , gpu_batch_buffer_cursor(0)
    , gpu_batch_buffer_size()
    , gpu_batch_buffer(nullptr)
    , gpu_movie_baselines(nullptr)
    , gpu_user_baselines(nullptr)
    , baseline_regularization_factor()
    // , gpu_implicit_feedback_factors(nullptr)
{
    gpuErrChk(cudaMalloc((void**) &this->gpu_self, sizeof (PredictionModel)));

    assert(!Data::batches.empty());
    this->gpu_batch_buffer_size = Data::batches.size() / k_batch_subgroup_count;

    gpuErrChk(cudaMalloc((void**) &this->gpu_batch_buffer,
        this->gpu_batch_buffer_size * sizeof (Data::Batch)));
}

PredictionModel::~PredictionModel()
{
    if (this->gpu_batch_buffer != nullptr)
    {
        gpuErrChk(cudaFree(this->gpu_batch_buffer));
        this->gpu_batch_buffer = nullptr;
    }

    if (this->gpu_movie_features != nullptr)
    {
        gpuErrChk(cudaFree(this->gpu_movie_features));
    }
    if (this->gpu_user_features != nullptr)
    {
        gpuErrChk(cudaFree(this->gpu_user_features));
    }
    if (this->gpu_test_data != nullptr)
    {
        gpuErrChk(cudaFree(this->gpu_test_data));
    }
    if (this->gpu_rmse != nullptr)
    {
        gpuErrChk(cudaFree(this->gpu_rmse));
    }
    if (this->gpu_movie_baselines != nullptr)
    {
        gpuErrChk(cudaFree(this->gpu_movie_baselines));
    }
    if (this->gpu_user_baselines != nullptr)
    {
        gpuErrChk(cudaFree(this->gpu_user_baselines));
    }
    // if (this->gpu_implicit_feedback_factors != nullptr)
    // {
    //     gpuErrChk(cudaFree(this->gpu_implicit_feedback_factors));
    // }
    gpuErrChk(cudaFree(this->gpu_self));
}

bool PredictionModel::_setup_next_gpu_batch_buffer()
{
    // printf("cursor: %d, size: %d, total size: %d\n",
    //     this->gpu_batch_buffer_cursor, this->gpu_batch_buffer_size,
    //     Data::batches.size());
    // printf("cudaMemcpy-ing from %p to %p [%p]\n",
    //     Data::batches.data() + this->gpu_batch_buffer_cursor,
    //     Data::batches.data() + this->gpu_batch_buffer_cursor + this->gpu_batch_buffer_size,
    //     Data::batches.data() + Data::batches.size());
    gpuErrChk(cudaMemcpy(this->gpu_batch_buffer,
        Data::batches.data() + this->gpu_batch_buffer_cursor,
        this->gpu_batch_buffer_size * sizeof (Data::Batch),
        cudaMemcpyHostToDevice));
    this->gpu_batch_buffer_cursor += this->gpu_batch_buffer_size;
    if (this->gpu_batch_buffer_cursor >= Data::batches.size())
    {
        this->gpu_batch_buffer_cursor = 0;
    }
    return this->gpu_batch_buffer_cursor == 0;
}

void PredictionModel::_learning_finished_callback()
{
    assert(this->gpu_movie_features != nullptr);
    assert(this->gpu_user_features != nullptr);
    assert(this->gpu_movie_baselines != nullptr);
    assert(this->gpu_user_baselines != nullptr);
    // assert(this->gpu_implicit_feedback_factors != nullptr);

    // Free GPU memory
    gpuErrChk(cudaFree(this->gpu_movie_features));
    gpuErrChk(cudaFree(this->gpu_user_features));
    gpuErrChk(cudaFree(this->gpu_movie_baselines));
    gpuErrChk(cudaFree(this->gpu_user_baselines));
    // gpuErrChk(cudaFree(this->gpu_implicit_feedback_factors));
    this->gpu_movie_features = nullptr;
    this->gpu_user_features = nullptr;
    this->gpu_movie_baselines = nullptr;
    this->gpu_user_baselines = nullptr;
    // this->gpu_implicit_feedback_factors = nullptr;

    this->initialized = false;
}

void PredictionModel::_finished_epoch_callback()
{
    assert(this->gpu_movie_features != nullptr);
    assert(this->gpu_user_features != nullptr);

    // Move buffer
    memcpy(this->movie_features.data(), this->movie_features_buffer.data(),
        this->movie_features.size() * sizeof (float));
    memcpy(this->user_features.data(), this->user_features_buffer.data(),
        this->user_features.size() * sizeof (float));

    // Copy the results back out from the GPU
    gpuErrChk(cudaMemcpy(this->movie_features_buffer.data(),
        this->gpu_movie_features, this->movie_features.size() * sizeof (float),
        cudaMemcpyDeviceToHost));
    gpuErrChk(cudaMemcpy(this->user_features_buffer.data(),
        this->gpu_user_features, this->user_features.size() * sizeof (float),
        cudaMemcpyDeviceToHost));
}

void PredictionModel::init(int feature_vec_dim, float learning_rate,
    float regularization_factor, float baseline_regularization_factor)
{
    assert(feature_vec_dim > 0);

    assert(!this->initialized);
    assert(this->gpu_movie_features == nullptr);
    assert(this->gpu_user_features == nullptr);
    assert(this->gpu_movie_baselines == nullptr);
    assert(this->gpu_user_baselines == nullptr);
    // assert(this->gpu_implicit_feedback_factors == nullptr);

    this->learning_rate = learning_rate;
    this->regularization_factor = regularization_factor;
    this->baseline_regularization_factor = baseline_regularization_factor;

    this->gpu_batch_buffer_cursor = 0;

    // Initialize the feature vectors
    this->feature_vec_dim = feature_vec_dim;
    movie_features.resize(Data::k_movie_count * this->feature_vec_dim);
    user_features.resize(Data::k_user_count * this->feature_vec_dim);
    movie_features_buffer.resize(Data::k_movie_count * this->feature_vec_dim);
    user_features_buffer.resize(Data::k_user_count * this->feature_vec_dim);

    std::srand(std::time(0));

    auto randomSet = [](float &val) { return std::rand() / float(RAND_MAX); };
    std::transform(this->movie_features_buffer.begin(),
        this->movie_features_buffer.end(), this->movie_features_buffer.begin(),
        randomSet);
    std::transform(this->user_features_buffer.begin(),
        this->user_features_buffer.end(), this->user_features_buffer.begin(),
        randomSet);

    std::array<float, Data::k_movie_count + 1> movie_feature_mags;
    std::array<float, Data::k_user_count + 1> user_feature_mags;
    std::fill(movie_feature_mags.begin(), movie_feature_mags.end(), 0.0);
    std::fill(user_feature_mags.begin(), user_feature_mags.end(), 0.0);

    for (int movie_id = 0; movie_id < Data::k_movie_count; movie_id++)
    {
        for (int basis_id = 0; basis_id < this->feature_vec_dim; basis_id++)
        {
            float comp =
                this->movie_features_buffer.at(_index_mf(movie_id, basis_id));
            movie_feature_mags[movie_id] += comp * comp;
        }
    }

    for (int user_id = 0; user_id < Data::k_user_count; user_id++)
    {
        for (int basis_id = 0; basis_id < this->feature_vec_dim; basis_id++)
        {
            float comp =
                this->user_features_buffer.at(_index_uf(user_id, basis_id));
            user_feature_mags[user_id] += comp * comp;
        }
    }

    auto sqrter = [](float &mag) { return sqrt(mag); };
    std::transform(movie_feature_mags.begin(), movie_feature_mags.end(),
        movie_feature_mags.begin(), sqrter);
    std::transform(user_feature_mags.begin(), user_feature_mags.end(),
        user_feature_mags.begin(), sqrter);

    for (int basis_id = 0; basis_id < this->feature_vec_dim; basis_id++)
    {
        this->movie_features_buffer.at(_index_mf(0, basis_id)) = 0.0;
    }
    for (int movie_id = 1; movie_id < Data::k_movie_count; movie_id++)
    {
        for (int basis_id = 0; basis_id < this->feature_vec_dim; basis_id++)
        {
            this->movie_features_buffer.at(_index_mf(movie_id, basis_id)) /=
                movie_feature_mags[movie_id];
        }
    }

    for (int basis_id = 0; basis_id < this->feature_vec_dim; basis_id++)
    {
        this->user_features_buffer.at(_index_uf(0, basis_id)) = 0.0;
    }
    for (int user_id = 1; user_id < Data::k_user_count; user_id++)
    {
        for (int basis_id = 0; basis_id < this->feature_vec_dim; basis_id++)
        {
            this->user_features_buffer.at(_index_uf(user_id, basis_id)) /=
                user_feature_mags[user_id];
        }
    }

    memcpy(this->movie_features.data(), this->movie_features_buffer.data(),
        this->movie_features.size() * sizeof (float));
    memcpy(this->user_features.data(), this->user_features_buffer.data(),
        this->user_features.size() * sizeof (float));

    // std::for_each(this->movie_features.begin(), this->movie_features.end(),
    //     [](float &val){ printf("%f, ", val); });
    // printf("\n");
    // std::for_each(this->user_features.begin(), this->user_features.end(),
    //     [](float &val){ printf("%f, ", val); });
    // printf("\n");

    // Setup CUDA GPU memory
    gpuErrChk(cudaMalloc((void**) &this->gpu_movie_features,
        this->movie_features_buffer.size() * sizeof (float)));
    gpuErrChk(cudaMalloc((void**) &this->gpu_user_features,
        this->user_features_buffer.size() * sizeof (float)));
    gpuErrChk(cudaMalloc((void**) &this->gpu_test_data,
        Data::k_test_count * sizeof (Data::Entry)));
    gpuErrChk(cudaMalloc((void**) &this->gpu_rmse, sizeof (float)));
    gpuErrChk(cudaMalloc((void**) &this->gpu_movie_baselines,
        Data::k_movie_count * sizeof (float)));
    gpuErrChk(cudaMalloc((void**) &this->gpu_user_baselines,
        Data::k_user_count * sizeof (float)));
    // gpuErrChk(cudaMalloc((void**) &this->gpu_implicit_feedback_factors,
    //     Data::k_movie_count * this->feature_vec_dim * sizeof (float)));

    gpuErrChk(cudaMemcpy(this->gpu_movie_features,
        this->movie_features_buffer.data(),
        this->movie_features_buffer.size() * sizeof (float),
        cudaMemcpyHostToDevice));
    gpuErrChk(cudaMemcpy(this->gpu_user_features,
        this->user_features_buffer.data(),
        this->user_features_buffer.size() * sizeof (float),
        cudaMemcpyHostToDevice));
    gpuErrChk(cudaMemcpy(this->gpu_test_data, Data::test.data(),
        Data::k_test_count * sizeof (Data::Entry), cudaMemcpyHostToDevice));
    gpuErrChk(cudaMemset(this->gpu_movie_baselines, 0,
        Data::k_movie_count * sizeof (float)));
    gpuErrChk(cudaMemset(this->gpu_user_baselines, 0,
        Data::k_user_count * sizeof (float)));
    // gpuErrChk(cudaMemset(this->gpu_implicit_feedback_factors, 0,
    //     Data::k_movie_count * this->feature_vec_dim * sizeof (float)));

    std::vector<float> movie_baselines(Data::k_movie_count);
    std::vector<float> user_baselines(Data::k_user_count);
    std::transform(movie_baselines.begin(), movie_baselines.end(),
        movie_baselines.begin(), [](float &val) { return 0.0; });
    std::transform(user_baselines.begin(), user_baselines.end(),
        user_baselines.begin(), [](float &val) { return 0.0; });
    gpuErrChk(cudaMemcpy(this->gpu_movie_baselines, movie_baselines.data(),
        Data::k_movie_count * sizeof (float), cudaMemcpyHostToDevice));
    gpuErrChk(cudaMemcpy(this->gpu_user_baselines, user_baselines.data(),
        Data::k_user_count * sizeof (float), cudaMemcpyHostToDevice));

    this->initialized = true;

    // Setup self in the GPU memory
    gpuErrChk(cudaMemcpy(this->gpu_self, this, sizeof (PredictionModel),
        cudaMemcpyHostToDevice));
}

CUDA_CALLABLE float PredictionModel::predictGPU(const int user_id,
    const int movie_id) const
{
    assert(this->initialized);
    assert(this->gpu_movie_features != nullptr);
    assert(this->gpu_user_features != nullptr);
    float prediction = 0.0;
    for (int basis_id = 0; basis_id < this->feature_vec_dim; basis_id++)
    {
        prediction += this->gpu_movie_features[_index_mf(movie_id, basis_id)]
            * this->gpu_user_features[_index_uf(user_id, basis_id)];
    }
    return prediction;
}

float PredictionModel::predictCPU(const int user_id, const int movie_id) const
{
    float prediction = 0.0;
    for (int basis_id = 0; basis_id < this->feature_vec_dim; basis_id++)
    {
        prediction += this->movie_features[_index_mf(movie_id, basis_id)]
            * this->user_features[_index_uf(user_id, basis_id)];
    }
    return prediction;
}

CUDA_CALLABLE void PredictionModel::learnGPU(const int user_id,
    const int movie_id, const int rating) const
{
    assert(this->initialized);
    assert(this->gpu_movie_features != nullptr);
    assert(this->gpu_user_features != nullptr);

    float negative_gradient_scale = this->learning_rate
        * (rating - this->predictGPU(user_id, movie_id));

    for (int basis_id = 0; basis_id < this->feature_vec_dim; basis_id++)
    {
        float &mf = this->gpu_movie_features[_index_mf(movie_id, basis_id)];
        float &uf = this->gpu_user_features[_index_uf(user_id, basis_id)];
        uf += negative_gradient_scale * mf;
        mf += negative_gradient_scale * uf;
    }
}

void PredictionModel::learnCPU(const int user_id, const int movie_id,
    const int rating)
{
    assert(!this->initialized);
    assert(this->gpu_movie_features == nullptr);
    assert(this->gpu_user_features == nullptr);

    float negative_gradient_scale = this->learning_rate
        * (rating - this->predictCPU(user_id, movie_id));

    for (int basis_id = 0; basis_id < this->feature_vec_dim; basis_id++)
    {
        this->user_features_buffer.at(_index_uf(user_id, basis_id)) +=
            negative_gradient_scale
                * this->movie_features_buffer.at(_index_mf(movie_id, basis_id));
        this->movie_features_buffer.at(_index_mf(movie_id, basis_id)) +=
            negative_gradient_scale
                * this->user_features_buffer.at(_index_uf(user_id, basis_id));
    }
}

CUDA_KERNEL void cudaComputeRMSE(const unsigned int feature_vec_dim,
    const unsigned int test_count, const Data::Entry *test_data,
    const float *movie_features, const float *user_features, float *rmse)
{
    // First third is the movie feature vector
    // Second third is the user feature vector
    // Final third is used as a buffer for doing computations (e.g. dot product)
    // Last element is for accumulating rmse
    extern __shared__ float feature_buffer[];
    const unsigned int k_offset_mf = 0;
    const unsigned int k_offset_uf = feature_vec_dim;
    const unsigned int k_offset_buffer = 2 * feature_vec_dim;
    const unsigned int k_offset_rmse = 3 * feature_vec_dim;

    const unsigned int cursor = threadIdx.x;

    if (cursor == 0)
    {
        feature_buffer[k_offset_rmse] = 0.0;
    }

    // Process one entry per block at a time
    unsigned int entry_id = blockIdx.x;
    while (entry_id < test_count)
    {
        const Data::Entry &entry = test_data[entry_id];

        // Entry should not be empty
        assert(entry.user_id != 0);
        assert(entry.movie_id != 0);

        // Load the feature vectors into feature_buffer
        if (cursor < feature_vec_dim)
        {
            feature_buffer[k_offset_mf + cursor] =
                movie_features[_index_mf(entry.movie_id, cursor)];
            feature_buffer[k_offset_uf + cursor] =
                user_features[_index_uf(entry.user_id, cursor)];
        }

        __syncthreads();

        // Take a dot product (using reduction) and use the result as the
        // prediction
        if (cursor < feature_vec_dim)
        {
            feature_buffer[k_offset_buffer + cursor] =
                feature_buffer[k_offset_mf + cursor]
                    * feature_buffer[k_offset_uf + cursor];
        }

        __syncthreads();

        unsigned int reduction_range = feature_vec_dim;
        while (reduction_range > 1)
        {
            unsigned int stride = reduction_range / 2 + reduction_range % 2;
            if (cursor < reduction_range / 2)
            {
                unsigned int left_index = k_offset_buffer + cursor;
                unsigned int right_index = k_offset_buffer + cursor + stride;
                assert(left_index != right_index);
                feature_buffer[left_index] += feature_buffer[right_index];
            }

            reduction_range = stride;
            __syncthreads();
        }

        if (cursor == 0)
        {
            float error = entry.rating - feature_buffer[k_offset_buffer + 0];
            // atomicAdd(&feature_buffer[k_offset_rmse], error * error);
            feature_buffer[k_offset_rmse] += error * error;
        }

        entry_id += gridDim.x;
    }

    if (cursor == 0)
    {
        atomicAdd(rmse, feature_buffer[k_offset_rmse]);
    }
}

float PredictionModel::testRMSE()
{
    // float rmse = 0.0;
    // printf("Computing RMSE");
    // std::for_each(Data::test.begin(), Data::test.end(),
    //     [&, count = 0](const Data::Entry &entry) mutable
    //     {
    //         float diff = entry.rating
    //             - this->predictCPU(entry.user_id, entry.movie_id);
    //         rmse += diff * diff;

    //         count++;
    //         if (count % 100000)
    //         {
    //             printf(".");
    //             std::cout.flush();
    //         }
    //     });
    // printf("DONE\n");

    // float rmse = 0.0;
    // int count = 0;
    // printf("Computing RMSE");
    // for (const Data::Entry &entry : Data::test)
    // {
    //     float diff = entry.rating
    //         - this->predictCPU(entry.user_id, entry.movie_id);
    //     rmse += diff * diff;

    //     count++;
    //     if (count % 100000 == 0)
    //     {
    //         printf(".");
    //         std::cout.flush();
    //     }
    // }
    // printf("DONE\n");

    const unsigned int thread_per_block = _geq_pow_of_2(this->feature_vec_dim);
    const unsigned int blocks_per_grid = 1024;
    const unsigned int shmem_size = (this->feature_vec_dim * 3 + 1)
        * sizeof (float);

    // printf("block size: %u\n", thread_per_block);
    // printf("block count: %u\n", blocks_per_grid);
    // printf("shmem size: %u\n", shmem_size);

    float cuda_rmse = 0.0;
    gpuErrChk(cudaMemcpy(this->gpu_rmse, &cuda_rmse, sizeof (float),
        cudaMemcpyHostToDevice));

    cudaComputeRMSE<<<blocks_per_grid, thread_per_block, shmem_size>>>(
        (unsigned int) this->feature_vec_dim, (unsigned int) Data::k_test_count,
            this->gpu_test_data, this->gpu_movie_features,
            this->gpu_user_features, this->gpu_rmse);

    gpuErrChk(cudaMemcpy(&cuda_rmse, this->gpu_rmse, sizeof (float),
        cudaMemcpyDeviceToHost));

    // printf("%f\n", rmse);
    // printf("%f\n", cuda_rmse);
    // printf("%f\n", cuda_rmse / rmse);
    // assert((rmse - cuda_rmse) * (rmse - cuda_rmse) < 0.0000001);

    return cuda_rmse;
}

/**************************** CUDA Gradient Descent ***************************/

CUDA_KERNEL void batchedGradientDescentKernel_naive(
    const PredictionModel *model, const Data::Batch *batches,
    const unsigned int batch_id)
{
    const unsigned int cursor = threadIdx.x + blockDim.x * blockIdx.x;

    const Data::Entry &entry =
        reinterpret_cast<const Data::Entry*>(batches + batch_id)[cursor];
    if (entry.user_id == 0)
    {
        assert(entry.movie_id == 0);
        return;
    }
    model->learnGPU(entry.user_id, entry.movie_id, entry.rating);
}

CUDA_KERNEL void batchedGradientDescentKernel(
    const unsigned int feature_vec_dim, const float learning_rate,
    const float regularization_factor, float *movie_features,
    float *user_features, const Data::Batch *batches,
    const unsigned int batch_id, const float overall_rating_avg,
    float *movie_baselines, float *user_baselines,
    const float baseline_regularization_factor)
{
    // First third is the movie feature vector
    // Second third is the user feature vector
    // Final third is used as a buffer for doing computations (e.g. dot product)
    extern __shared__ float feature_buffer[];
    const unsigned int k_offset_mf = 0;
    const unsigned int k_offset_uf = feature_vec_dim;
    const unsigned int k_offset_buffer = 2 * feature_vec_dim;

    // Process one entry per block
    const unsigned int entry_id = blockIdx.x;
    const Data::Entry &entry =
        reinterpret_cast<const Data::Entry*>(batches + batch_id)[entry_id];

    // If empty entry in batch, do nothing
    if (entry.user_id == 0)
    {
        assert(entry.movie_id == 0);
        return;
    }

    const unsigned int cursor = threadIdx.x;

    // Load the feature vectors into feature_buffer
    if (cursor < feature_vec_dim)
    {
        feature_buffer[k_offset_mf + cursor] =
            movie_features[_index_mf(entry.movie_id, cursor)];
        feature_buffer[k_offset_uf + cursor] =
            user_features[_index_uf(entry.user_id, cursor)];
    }

    __syncthreads();

    // Take a dot product (using reduction) and use the result as the prediction
    if (cursor < feature_vec_dim)
    {
        feature_buffer[k_offset_buffer + cursor] =
            feature_buffer[k_offset_mf + cursor]
                * feature_buffer[k_offset_uf + cursor];
    }

    __syncthreads();

    // float dot_product = 0.0;
    // for (unsigned int i = 0; i < feature_vec_dim; i++)
    // {
    //     dot_product += feature_buffer[i + k_offset_buffer];
    // }

    // __syncthreads();

    unsigned int reduction_range = feature_vec_dim;
    while (reduction_range > 1)
    {
        unsigned int stride = reduction_range / 2 + reduction_range % 2;
        if (cursor < reduction_range / 2)
        {
            unsigned int left_index = k_offset_buffer + cursor;
            unsigned int right_index = k_offset_buffer + cursor + stride;
            assert(left_index != right_index);
            feature_buffer[left_index] += feature_buffer[right_index];
        }

        reduction_range = stride;
        __syncthreads();
    }

    // assert((dot_product - feature_buffer[k_offset_buffer + 0])
    //     * (dot_product - feature_buffer[k_offset_buffer + 0]) < 0.00000001);
    // __syncthreads();

    float movie_baseline = movie_baselines[entry.movie_id];
    float user_baseline = user_baselines[entry.user_id];
    float prediction = feature_buffer[k_offset_buffer + 0];
        // + movie_baseline + user_baseline + overall_rating_avg;
    float error = entry.rating - prediction;

    // Learn!
    if (cursor < feature_vec_dim)
    {
        movie_baseline += learning_rate
            * (error - baseline_regularization_factor * movie_baseline);
        user_baseline += learning_rate
            * (error - baseline_regularization_factor * user_baseline);
        float &mf = feature_buffer[k_offset_mf + cursor];
        float &uf = feature_buffer[k_offset_uf + cursor];
        uf += learning_rate * (error * mf - regularization_factor * uf);
        mf += learning_rate * (error * uf - regularization_factor * mf);

        movie_baselines[entry.movie_id] = movie_baseline;
        user_baselines[entry.user_id] = user_baseline;
        movie_features[_index_mf(entry.movie_id, cursor)] = mf;
        user_features[_index_uf(entry.user_id, cursor)] = uf;
    }
}

void PredictionModel::cudaLearnBatches()
{
    const unsigned int thread_per_block = _geq_pow_of_2(this->feature_vec_dim);
    const unsigned int blocks_per_grid = Data::k_batch_size;
    const unsigned int shmem_size = this->feature_vec_dim * 3 * sizeof (float);

    // printf("block size: %u\n", thread_per_block);
    // printf("block count: %u\n", blocks_per_grid);
    // printf("shmem size: %u\n", shmem_size);

    cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);

    int epoch_count = 0;
    float prev_rmse = FLT_MAX;
    float curr_rmse = FLT_MAX * 0.9;
    while (curr_rmse < prev_rmse)
    {
        float learning_rate_scale = k_learning_rate_scalar
            * (prev_rmse - curr_rmse) / prev_rmse;
        learning_rate_scale = learning_rate_scale > k_max_learning_rate_scale ?
            k_max_learning_rate_scale : learning_rate_scale;
        learning_rate_scale = learning_rate_scale < k_min_learning_rate_scale ?
            k_min_learning_rate_scale : learning_rate_scale;
        printf("\nEPOCH %d\n", epoch_count);
        printf("learning_rate_scale = %f\n", learning_rate_scale);
        printf("learning");
        std::cout.flush();
        bool finished_with_epoch = false;
        time_t start_time = std::time(0);
        int counter = 0;
        while (!finished_with_epoch)
        {
            finished_with_epoch = this->_setup_next_gpu_batch_buffer();
            for (unsigned int batch_id = 0;
                batch_id < this->gpu_batch_buffer_size; batch_id++)
            {
                batchedGradientDescentKernel<<<blocks_per_grid,
                    thread_per_block, shmem_size>>>(
                        (unsigned int) this->feature_vec_dim,
                        this->learning_rate * learning_rate_scale,
                        this->regularization_factor,
                        this->gpu_movie_features, this->gpu_user_features,
                        this->gpu_batch_buffer, batch_id,
                        Data::overall_rating_avg, this->gpu_movie_baselines,
                        this->gpu_user_baselines,
                        this->baseline_regularization_factor);
            }
            if (++counter % 10 == 0)
            {
                printf(".");
                std::cout.flush();
            }
        }
        printf("DONE\n");
        printf("Took: %.f seconds\n", difftime(std::time(0), start_time));
        this->_finished_epoch_callback();
        epoch_count++;

        prev_rmse = curr_rmse;
        curr_rmse = this->testRMSE();
        printf("NEW RMSE: %f\n", sqrt(curr_rmse / Data::k_test_count));
    }

    this->_learning_finished_callback();
}


} // END namespace GradientDescent
