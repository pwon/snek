#pragma once

#include "common.hpp"
#include "common_cuda.hpp"

#include "data.hpp"

namespace GradientDescent
{
    class PredictionModel
    {
    private:
        static constexpr int k_batch_subgroup_count = 1000;
        static constexpr int k_default_dim = 40;
        static constexpr float k_default_learning_rate = 0.001;
        static constexpr float k_learning_rate_scalar = 1000.0;
        static constexpr float k_max_learning_rate_scale = 10.0;
        static constexpr float k_min_learning_rate_scale = 1.0;
        static constexpr float k_default_regularization_factor = 0.005;
        static constexpr float k_deafult_baseline_regularization_factor = 0.005;

        int feature_vec_dim;
        std::vector<float> movie_features;
        std::vector<float> user_features;
        std::vector<float> movie_features_buffer;
        std::vector<float> user_features_buffer;
        float learning_rate;
        float regularization_factor;

        bool initialized;
        PredictionModel *gpu_self;
        float *gpu_movie_features;
        float *gpu_user_features;

        Data::Entry *gpu_test_data;
        float *gpu_rmse;

        int gpu_batch_buffer_cursor;
        int gpu_batch_buffer_size;
        Data::Batch *gpu_batch_buffer;

        float *gpu_movie_baselines;
        float *gpu_user_baselines;
        float baseline_regularization_factor;

        // float *gpu_implicit_feedback_factors;

        bool _setup_next_gpu_batch_buffer();
        void _finished_epoch_callback();
        void _learning_finished_callback();

    public:
        PredictionModel();
        ~PredictionModel();

        // Use by calling:
        //     Setup Data
        //     {
        //         [this]->init();
        //         cudaLearnBatches([this]);
        //         ^calls learningFinishCallback(); when complete
        //         [this]->learningFinishCallback();
        //     } <repeat desired number of times>
        void init(int feature_vec_dim = k_default_dim,
            float learning_rate = k_default_learning_rate,
            float regularization_factor = k_default_regularization_factor,
            float baseline_regularization_factor
                = k_deafult_baseline_regularization_factor);

        CUDA_CALLABLE float predictGPU(const int user_id, const int movie_id)
            const;
        float predictCPU(const int user_id, const int movie_id) const;
        CUDA_CALLABLE void learnGPU(const int user_id, const int movie_id,
            const int rating) const;
        void learnCPU(const int user_id, const int movie_id, const int rating);

        float testRMSE();

        void cudaLearnBatches();
    };

}
