
from optparse import OptionParser

from pkg_python.Processor_zero import process as process_zero
from pkg_python.Processor_gradient_descent_0 import process as process_gd0
from pkg_python.Processor_independent_batching import process as process_ib
from pkg_python.Processor_clustering_0 import process as process_c0
# Add more processor scripts here


def main():
    # Parse commandline arguments
    usage_msg = "%script [options] training_datafile training_data_indexfile \
qual_file output_file [any other necessary files]"
    option_parser = OptionParser(usage=usage_msg)
    option_parser.add_option("-t", "--type", action="store",
        dest="process_type")
    option_parser.add_option("-o", "--order", action="store",
        dest="submission_order", default="mu")
    (options, args) = option_parser.parse_args()

    # if len(args) < 4:
    #     option_parser.print_help()
    #     print
    #     exit(1)

    load_processor_type = {
        None: process_zero,     # default
        "zero": process_zero,
        "gd0": process_gd0,
        "ib": process_ib,
        "c0": process_c0
        # Add entries for more process functions here
    }

    load_processor_type[options.process_type.lower()](
        options.submission_order.lower(), args)

if __name__ == '__main__':
    main()

