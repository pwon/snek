// Implements different neighborhood models.

#include "neighborhood.hpp"

#define NITEMS 17770
#define NUSERS 458293

int index(int i, int j)
{
	return (NITEMS) * i + j;
}

std::vector<int> TopK(std::vector<float> &list, int K)
{
	// Implmentation in std::priority_queue is a max priority queue. We want
	// to use a min priority queue instead, so take the negatives of all
	// the values in list.
	std::priority_queue<std::pair<float, int>> Q;
	for (int i = 0; i < list.size(); i++)
	{
		// Q.push(std::pair<float, int>(list[i], i));
		if (Q.size() < K)
		{
			Q.push(std::pair<float, int>(-1.0 * list[i], i));
		}
		else if (list[i] > -1.0 * Q.top().second)
		{
			Q.pop();
			Q.push(std::pair<float, int>(-1.0 * list[i], i));
		}
	}
	std::vector<int> topk_indices;
	for (int i = 0; i < K; i++)
	{
		int idx = Q.top().second;
		topk_indices.push_back(idx);
		Q.pop();
	}
	return topk_indices;
}

float sigmoid(float x)
{
	return (float) 1.0 / (1.0 + exp(-1 * (double) x));
}

float kNNBasic(Data::Entry e, std::vector<float> &S, std::unordered_map<std::string, float> &ratings, int K)
{
	std::vector<float> sims(NITEMS);
	for (int i = 0; i < NITEMS; ++i)
	{
		sims[i] = S[index(e.movie_id - 1, i)];
	}
	// Get the indices of the K nearest neighbors of our item to rate, which has id movie_id.
	std::vector<int> topk = TopK(sims, K);
	float wsum = 0.0;
	float norm = 0.0;
	for (int i = 0; i < K; i++)
	{
		// User and movie indices are 1-indexed.
		std::string key = std::to_string(e.user_id) + "," + std::to_string(topk[i] + 1);
		if (ratings.find(key) != ratings.end())
		{
			wsum += S[index(e.movie_id - 1, topk[i])] * ratings[key];
			norm += S[index(e.movie_id - 1, topk[i])];
			// wsum += S[e.movie_id - 1][topk[i]] * ratings[key];
			// norm += S[e.movie_id - 1][topk[i]];
		}
	}
	return (norm != 0.0) ? (wsum / norm) : 0.0;
}

float kNNMovie(Data::Entry e, std::vector<float> &S, std::unordered_map<std::string, float> &ratings, int K, float delta, float gamma)
{
	std::vector<float> sims(NITEMS);
	for (int i = 0; i < NITEMS; ++i)
	{
		sims[i] = S[index(e.movie_id - 1, i)];
	}
	// Get the indices of the K nearest neighbors of our item to rate, which has id movie_id.
	std::vector<int> topk = TopK(sims, K);
	float wsum = 0.0;
	float norm = 0.0;
	for (int i = 0; i < K; i++)
	{
		std::string key = std::to_string(e.user_id) + "," + std::to_string(topk[i] + 1);
		if (ratings.find(key) != ratings.end())
		{
			// float new_sim = sigmoid(delta * S[e.movie_id - 1][topk[i]] + gamma); // 1-indexing
			float new_sim = sigmoid(delta * S[index(e.movie_id - 1, topk[i])] + gamma);
			wsum += new_sim * ratings[key];
			norm += new_sim;
		}
	}
	return (norm != 0.0) ? (wsum / norm) : 0.0;
}

float kNNMovieV3(Data::Entry e, std::vector<float> &S, std::unordered_map<std::string, float> &ratings, std::unordered_map<std::string, float> &times, int K,
	float delta, float gamma, float beta)
{
	std::vector<float> sims(NITEMS);
	for (int i = 0; i < NITEMS; ++i)
	{
		sims[i] = S[index(e.movie_id - 1, i)];
	}
	std::vector<int> topk = TopK(sims, K);
	float wsum = 0.0;
	float norm = 0.0;
	for (int i = 0; i < K; i++)
	{
		std::string key = std::to_string(e.user_id) + "," + std::to_string(topk[i] + 1);
		if (ratings.find(key) != ratings.end() or times.find(key) != times.end())
		{
			float delta_t = abs(e.date_id - times[key]);
			float time_discount = exp(-1 * (delta_t / beta));
			// float sim_date = sigmoid((delta * S[e.movie_id][topk[i]] * time_discount) + gamma);
			float sim_date = sigmoid((delta * S[index(e.movie_id, topk[i])] * time_discount) + gamma);
			wsum += sim_date * ratings[key];
			norm += sim_date;
		}
	}
	return (norm != 0.0) ? (wsum / norm) : 0.0;
}
