// Header file for kNN models.
#pragma once

#include <math.h>
#include <queue>
#include <string>
#include <utility>
#include "common.hpp"
#include "data.hpp"

// Helper function: turns a 2D index into a 1D index.
int index(int i, int j);

// Helper function; gets the indices of the K largest values from a list.
std::vector<int> TopK(std::vector<float> &list, int K);

// Basic neighborhood model: predict rating using Top K neighbors' ratings
// weighted by similarity.
float kNNBasic(Data::Entry e, std::vector<float> &S, std::unordered_map<std::string, float> &ratings, int K);

// An extension of the basic model in which we use a sigmoid to rescale the
// weights. Has hyperparameters delta and gamma.
float kNNMovie(Data::Entry e, std::vector<float> &S, std::unordered_map<std::string, float> &ratings, int K, float delta, float gamma);

// An extension of our sigmoid re-weighting model, which now does exponential
// time discounting of weightings. The further a movie is in the past, the
// more we discount it. Has an additional hyperparameter beta.
float kNNMovieV3(Data::Entry e, std::vector<float> &S, std::unordered_map<std::string, float> &ratings, int K, float delta, float gamma, float beta);

// TODO: Koren Interpolation weights algorithm
// TODO: BigChaos models V6, V&???
