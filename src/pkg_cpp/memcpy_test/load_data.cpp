#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <array>
using namespace std;
#include <cstdio>
#include <ctime>

// do memcpy, set up a main that will pull everything and download it into a file in memory
// and then make another function which is able to pull data from it 

// try making a small test thing to call first to see if it works

#define N 102416306   // total number of data points in all.dta

struct DataPoint {
    int user_id;
    int movie_id;
    int date_id;
    int rating;
};

void usage_error(string name) {
    cout << "usage: " << name << " in_file out_file\n";
    exit(EXIT_FAILURE);
};

// Usage: [thisname] in_file out_file
int main(int argc, char const *argv[])
{
    string line;
    // char dest[100];
    ifstream in_file;
    ofstream out_file;

    // DataPoint data_array[102416306];

    std::clock_t start;
    double duration;

    start = std::clock();
    cout << "start clock\n";

    if (argc < 3) {
        usage_error(argv[0]);
    }

    in_file.open(argv[1]);
    if (!in_file.is_open()) {
        cout << "Unable to open in_file";
        usage_error(argv[0]);
    }

    out_file.open(argv[2]);
    if (!out_file.is_open()) {
        in_file.close();
        cout << "Unable to open out_file";
        usage_error(argv[0]);
    }

    cout << "start loop\n";

    // Read in all of the data to the array
    int i = 0;
    DataPoint this_point;
    cout << sizeof(DataPoint) << "\n" ;
    while (getline(in_file, line) && (i < N)) {
        std::istringstream iss(line);
        int u;
        int m;
        int d;
        int r;
        if (iss >> u >> m >> d >> r) {
            this_point.user_id = u;
            this_point.movie_id = m;
            this_point.date_id = d;
            this_point.rating = r;
            // data_array[i] = this_point;

            char dest[sizeof(DataPoint)];
            memcpy(dest, &this_point, sizeof(this_point));
            out_file << *dest << "\n";
            i++;
        }
        // std::memcpy(dest, line.c_str(), sizeof line);
        // out_file << dest << '\n';
        cout << i << "\n";
    }
    in_file.close();
    // char *dest;
    for (i = 0; i < N; i++) {
        // memcpy(dest, &data_array, sizeof DataPoint);
    }
    out_file.close();

    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout << "duration: " << duration << '\n';

    return 0;
}

