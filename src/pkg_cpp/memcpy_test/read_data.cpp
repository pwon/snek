#include <iostream>
#include <fstream>
#include <istream>
#include <cstring>
#include <array>
using namespace std;
#include <cstdio>
#include <ctime>

#define N 20


struct DataPoint {
    int user_id;
    int movie_id;
    int date_id;
    int rating;
};

void usage_error(string name) {
    cout << "usage: " << name << " in_file\n";
    exit(EXIT_FAILURE);
};

DataPoint *read_from_file(char const *argv[]) {
    ifstream in_file;

    in_file.open(argv[1]);
    if (!in_file.is_open()) {
        cout << "Unable to open in_file\n";
        usage_error(argv[0]);
    }

    string line;
    DataPoint *ret = (DataPoint *) malloc(sizeof(DataPoint) * N);
    int i = 0;
    while (getline(in_file, line) && (i < N)) {
        memcpy(&(ret[i]), &line, sizeof(DataPoint));
        i++;
    }

    in_file.close();

    return ret;
}

// Usage: [thisname] in_file
int main(int argc, char const *argv[])
{
    // char dest[100];
    ifstream in_file;

    DataPoint *data_array;

    std::clock_t start;
    double duration;

    start = std::clock();
    cout << "start clock\n";

    if (argc < 2) {
        usage_error(argv[0]);
    }

    data_array = read_from_file(argv);

    for (int i = 0; i < N; i++) {
        cout << i << ": " << data_array[i].user_id ;
        cout << " " << data_array[i].movie_id ;
        cout << " " << data_array[i].date_id;
        cout << " " << data_array[i].rating << "\n";
    }

    free(data_array);

    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout << "duration: " << duration << '\n';

    return 0;
}