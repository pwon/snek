import sys

# The regularization factor for movie offsets.
movie_reg = 25
# The regularization factor for user offsets.
user_reg = 10

class User:
	'''
	Represents the rating history of a user. Keeps track of the movies a user
	has rated, as well as information such as the number of movies rated,
	the average rating, and the user bias from the average.
	'''
	def __init__(self):
		# Number of movies rated.
		self.count = 0
		# Average rating of the user.
		self.avg_rating = 0
		# The identities of the movies rated, as a list of movie IDs.
		self.rated = []
		# The offset from the global average rating.
		self.offset = 0

def process(datatype, train_filename, train_idxfilename, qual_filename, output_filename):
	print "Starting...\t\t\t"
	sys.stdout.flush()

	# All data seen so far.
	processed_data_count = 0

	# Count of base entries seen so far.
	count = 0
	# The global average rating.
	avg_rating = 0

	# A hashmap for storing movie information. It is keyed by movie ID and
	# stores a 2-tuple (count, avg_rating).
	movmap = dict()

	# A hashmap for storing user information. It is keyed by user ID and
	# stores a User object. Right now very memory intensive.
	usermap = dict()

	with open(train_filename, 'r') as training_datafile:
		with open(train_idxfilename, 'r') as training_dataidxfile:
			# Read all of the data indices from the index file. We will only
			# train on the base data (labeled "1") and use the rest for
			# validation and testing.
			data_indices = training_dataidxfile.readlines()

			# Iterate over the ratings in the data file.
			for entry in training_datafile:
				# Find the type of the entry.
				index = data_indices[processed_data_count]

				# Don't use entries other than base entries.
				if index.strip() != "1":
					processed_data_count += 1
					if processed_data_count % 100000 == 0:
						print "\rProcessing train data...\tcompleted: " \
							+ str(processed_data_count) + " data entries",
						sys.stdout.flush()
					continue

				user_id = -1
				movie_id = -1
				date_id = -1
				rating = -1
				if datatype == "mu":
					user_id, movie_id, date_id, rating = [int(x) for x in entry.split(" ")]
				elif datatype == "um":
					movie_id, user_id, date_id, rating = [int(x) for x in entry.split(" ")]
				else:
					raise Exception("Invalid submission type")

				# Update the global average rating.
				avg_rating = (count * avg_rating + rating)
				count += 1

				# Update the movie information.
				if movie_id in movmap:
					movcount, movrating = movmap[movie_id]
					movmap[movie_id][0] += 1
					movmap[movie_id][1] = (movcount * movrating + rating) / (movcount + 1)
				else:
					movmap[movie_id] = [1, rating]

				# Update the user information.
				if user_id in usermap:
					usermap[user_id].avg_rating = (usermap[user_id].avg_rating * usermap[user_id].count + rating) / (usermap[user_id].count + 1)
					usermap[user_id].count += 1
					usermap[user_id].rated.append(movie_id)
				else:
					user = User()
					user.avg_rating = rating
					user.count = 1
					user.rated = [movie_id]
					usermap[user_id] = user

				processed_data_count += 1
				if processed_data_count % 100000 == 0:
					print "\rProcessing train data...\tcompleted: " \
						+ str(processed_data_count) + " data entries",
					sys.stdout.flush()

			print "\rProcessing train data...\tDONE",
			print "Processed a total of" + str(processed_data_count) \
				+ " training data entries"

	# Now calculate the offsets for the movies.
	print "Calculating movie offsets..."

	for movid, movie in movmap.iteritems():
		offset = ((movie[0] * movie[1]) - (movie[0] * avg_rating)) / (movie_reg + movie[0])
		movmap[movid] = offset

	print "DONE"

	# Using the movie offsets, calculate the user offsets.
	print "Calculating user offsets..."

	for uid, user in usermap.iteritems():
		rating_sum = (user.count * user.avg_rating) - (user.count * avg_rating)
		for movid in user.rated:
			rating_sum -= movmap[movid]
		user.offset = ratin_sum / (user_reg + user.count)

	print "DONE"

	print ""

	# Now use our trained baseline predictor.
	with open(qual_filename, "r") as qual_datafile:
		with open(output_filename, "w") as output_datafile:
			processed_data_count = 0
			for entry in qual_datafile:
				user_id = -1
				movie_id = -1
				date_id = -1
				if datatype == "mu":
					user_id, movie_id, date_id = [int(x) for x in entry.split(' ')]
				elif datatype == "um":
					movie_id, user_id, date_id = [int(x) for x in entry.split(' ')]
				else:
					raise Exception("Invalid submission type")

				try:
					user_offset = usermap[user_id].offset
				except KeyError:
					user_offset = 0

				try:
					movie_offset = movmap[movie_id]
				except KeyError:
					movie_offset = 0

				rating = avg_rating + user_offset + movie_offset

				output_datafile.write(str(rating) + "\n")

				processed_data_count += 1
				if processed_data_count % 100000 == 0:
					print "\rProcessing qual data...\tcompleted: " \
						+ str(processed_data_count) + " data entries",
					sys.stdout.flush()

	print "\rProcessing test data...\tDONE"
	print "Processed a total of " + str(processed_data_count) \
		+ " qual data entries"

if __name__ == '__main__':
	raise Exception("Processor_*.py files are not executable python scripts")



