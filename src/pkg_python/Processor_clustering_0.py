# Processor that trains a kNN model according to Koren 2007.
import sys
import numpy as np

# Global parameters and hyperparameters.

# N.B. movies and users are 1-indexed
movie_count = 17770
user_count = 458293
#movies = [None]
#users = [None]

# K is a hyperparameter which controls how many of the closest neighbors we
# choose for our predictive model. Typical values of K are between 20 and 50.
K = 20

# Alpha is the shrinkage parameter for global effects. Should be small.
# alpha = 0.1

# Beta is the shrinkage parameter for learning interpolation weights.
# beta = 500

# Epsilon is the magnitude at which we'll stop our quadratic optimization.
epsilon = 0.01

# Max number of iterations for our quadratic optimizations, just to make sure
# it stops.
niter = 1000

afilename = "pkg_python/ahatS.txt"
sfilename = "pkg_python/corrcoefS.txt"

def quadOpt(A, b, err = epsilon, niter = niter):
    '''
    Given a K x K matrix A and a K x 1 vector b, minimizes

    x^TAx - 2b^T

    subject to the constraint that x >= 0.
    '''
    count = 0
    K = A.shape[0]
    # Make a row vector initialized with random valeus between 0 and 1.
    x = np.random.rand(K)
    # Do-While Loop
    while count < niter:
        # No need to transpose since NumPy does the "expected" thing.
        r = np.dot(A, x) - b
        # Set negative variables to 0.
        for i in range(K):
            if x[i] == 0 and r[i] < 0:
                r[i] = 0
        # Get the step size.
        step = np.dot(r, r) / float(np.dot(r, np.dot(A, r)))
        # Adjust the step size to prevent negative values.
        for i in range(K):
            if r[i] < 0:
                alpha = min(alpha, -1 * (x[i] / float(r[i])))
        x += alpha * r
        count += 1
        if np.linalg.norm(r) < epsilon:
            return x


def process(datatype, args):
    training_filename, training_indexfilename, qual_filename, output_filename = args

    print "Initializing...\t\t\t",
    sys.stdout.flush()
    #movies += [FilmTraits() for i in range(movie_count)]
    #users += [FilmTraits() for i in range(user_count)]
    #print "DONE!"

    # ratings = dict()
    # avg = 3.8 # Empirical average.
    avg = 0
    count = 0

    # Parse the training data into Python list of (uid, movid, dateid, rating)
    # quadruples.
    #train_data = []
    #test_data = []
    with open(training_filename, "r") as training_datafile:
        with open(training_indexfilename, "r") as training_dataindexfile:
            data_indices = training_dataindexfile.readlines()

            parsed_data_count = 0
            for entry in training_datafile:
                index = int(data_indices[parsed_data_count].strip())

                # Each entry is (type_0_id, type_1_id, date_id, rating)
                # e.g. If the input data is movies_to_users, then type_0_id is
                # user_id and type_1_id is movie_id.
                user_id = ""
                movie_id = ""
                date_id = ""
                rating = ""

                # Parse data
                if datatype == "mu":
                    user_id, movie_id, date_id, rating = entry.split(" ")
                elif datatype == "um":
                    movie_id, user_id, date_id, rating = entry.split(" ")
                else:
                    raise Exception("Invalid submission order type")
                # print index
                # print user_id + ", " + movie_id + ", " + date_id,
                # print ", " + rating

                data_entry = (int(user_id), int(movie_id), int(date_id),
                    int(rating))

                if index == 1:
                    #train_data += [data_entry]
                    ratings[user_id + "," + movie_id] = int(rating)
                    avg += int(rating)
                #elif index != 5:
                #    test_data += [data_entry]

                parsed_data_count += 1
                if parsed_data_count % 100000 == 0:
                    print "\rParsing data files...\tread: " \
                        + str(parsed_data_count) + " data entries",
                    sys.stdout.flush()

            print "\rParsing data files...\tDONE",
            print "                                                            "
            print "Parsed a total of " + str(parsed_data_count) \
                + " data entries"
            #print "Train Data Count: " + str(len(train_data))
            #print "Test Data Count: " + str(len(test_data))

    print "Average Rating is:", avg / float(count)

    #last_rmse = sys.float_info.max
    #new_rmse = sys.float_info.max
    # Training loop for SGD. REPLACE THIS STUFF------------------------------
    # Since we have pre-computed our A-bar and similarity matrices, we
    # don't need to do any training here. We'll just load in A-bar and s
    # into memory.
    A = np.genfromtxt(afilename)
    S = np.genfromtxt(sfilename)
    # END OF PROCESSOR-SPECIFIC TRAINING CODE (REPLACE)----------------------

    # Write out predictions to file.
    with open(qual_filename, "r") as qual_datafile:
        with open(output_filename, "w") as output_datafile:
            processed_data_count = 0
            for entry in qual_datafile:
                # Each entry is (type_0_id, type_1_id, date_id, rating)
                # e.g. If the input data is movies_to_users, then type_0_id is
                # user_id and type_1_id is movie_id.
                user_id = -1
                movie_id = -1
                date_id = -1
                if datatype == "mu":
                    user_id, movie_id, date_id = entry.split(" ")
                elif datatype == "um":
                    movie_id, user_id, date_id = entry.split(" ")
                else:
                    raise Exception("Invalid submission order type")
                # print user_id + ", " + movie_id + ", " + date_id

                user_id = int(user_id)
                movie_id = int(movie_id)
                date_id = int(date_id)

                # CODE TO PREDICT BASED ON MODEL: REPLACE--------------------
                # The weights when we want to predict user u rating item i is
                # given by Aw = b, where A is our precomputed A-hat matrix and
                # b is the ith row of A(-hat).
                b = A[movie_id - 1, :] #1-indexing 
                w = quadOpt(A, b)
                rating = 0
                # Get the top K neighboring items of item i.
                N = np.argpartition(S[i, :], movie_count - K)[-K:] # list of item indices
                for n in N:
                    if str(user_id) + "," + str(j + 1) in ratings:
                        rating += w[n] * ratings[str(user_id) + "," + str(j + 1)] # 1-indexing
                    else:
                        rating += w[n] * avg
                # REPLACE----------------------------------------------------

                output_datafile.write(str(rating) + "\n")

                processed_data_count += 1
                if processed_data_count % 10000 == 0:
                    print "\rProcessing qual data...\tcompleted: " \
                        + str(processed_data_count) + " data entries",
                    sys.stdout.flush()

            print "\rProcessing qual data...\tDONE",
            print "                                                            "
            print "Processed a total of " + str(processed_data_count) \
                + " qual data entries"


if __name__ == '__main__':
    raise Exception("Processor_*.py files are not executable python scripts")