
import sys

import numpy as np


class FilmTraits(object):
    """
    A class for an N-dim vector that represents the traits of a film or the film
    preferences of a user.
    """

    dim = 40

    def __init__(self):
        super(FilmTraits, self).__init__()
        self.traits = np.random.rand(FilmTraits.dim)
        self.traits /= np.linalg.norm(self.traits)

learning_rate = 0.001

# N.B. movies and users are 1-indexed
movie_count = 17770
user_count = 458293
movies = [None]
users = [None]

def train(user_id, movie_id, date_id, rating):
    global movies
    global users

    prediction = np.dot(users[user_id].traits, movies[movie_id].traits)
    negative_gradient_mag = learning_rate * (rating - prediction)

    users[user_id].traits += negative_gradient_mag * movies[movie_id].traits
    movies[movie_id].traits += negative_gradient_mag * users[user_id].traits


def process(datatype, args):
    training_filename, training_indexfilename, qual_filename, output_filename = args

    global movies
    global users

    print "Initializing...\t\t\t",
    sys.stdout.flush()
    movies += [FilmTraits() for i in range(movie_count)]
    users += [FilmTraits() for i in range(user_count)]
    print "DONE!"

    # parse out data
    train_data = []
    test_data = []
    with open(training_filename, "r") as training_datafile:
        with open(training_indexfilename, "r") as training_dataindexfile:
            data_indices = training_dataindexfile.readlines()

            parsed_data_count = 0
            for entry in training_datafile:
                index = int(data_indices[parsed_data_count].strip())

                # Each entry is (type_0_id, type_1_id, date_id, rating)
                # e.g. If the input data is movies_to_users, then type_0_id is
                # user_id and type_1_id is movie_id.
                user_id = ""
                movie_id = ""
                date_id = ""
                rating = ""

                # Parse data
                if datatype == "mu":
                    user_id, movie_id, date_id, rating = entry.split(" ")
                elif datatype == "um":
                    movie_id, user_id, date_id, rating = entry.split(" ")
                else:
                    raise Exception("Invalid submission order type")
                # print index
                # print user_id + ", " + movie_id + ", " + date_id,
                # print ", " + rating

                data_entry = (int(user_id), int(movie_id), int(date_id),
                    int(rating))

                if index == 1:
                    train_data += [data_entry]
                elif index != 5:
                    test_data += [data_entry]

                parsed_data_count += 1
                if parsed_data_count % 100000 == 0:
                    print "\rParsing data files...\tread: " \
                        + str(parsed_data_count) + " data entries",
                    sys.stdout.flush()

            print "\rParsing data files...\tDONE",
            print "                                                            "
            print "Parsed a total of " + str(parsed_data_count) \
                + " data entries"
            print "Train Data Count: " + str(len(train_data))
            print "Test Data Count: " + str(len(test_data))

    last_rmse = sys.float_info.max
    new_rmse = sys.float_info.max
    while last_rmse >= new_rmse:
        processed_data_count = 0
        for user_id, movie_id, date_id, rating in train_data:
            train(user_id, movie_id, date_id, rating)

            processed_data_count += 1
            if processed_data_count % 100000 == 0:
                print "\rTraining...\tprocessed: " \
                    + str(processed_data_count) + " data entries",
                sys.stdout.flush()

        print "\rTraining...\tDONE",
        print "                                                                "

        last_rmse = new_rmse
        new_rmse = 0.0

        for user_id, movie_id, date_id, rating in test_data:
            prediction = np.dot(users[user_id].traits,
                                movies[movie_id].traits)
            new_rmse += (prediction - rating)**2

        print "New RMSE = " + str(new_rmse)
    print ""

    with open(qual_filename, "r") as qual_datafile:
        with open(output_filename, "w") as output_datafile:
            processed_data_count = 0
            for entry in qual_datafile:
                # Each entry is (type_0_id, type_1_id, date_id, rating)
                # e.g. If the input data is movies_to_users, then type_0_id is
                # user_id and type_1_id is movie_id.
                user_id = -1
                movie_id = -1
                date_id = -1
                if datatype == "mu":
                    user_id, movie_id, date_id = entry.split(" ")
                elif datatype == "um":
                    movie_id, user_id, date_id = entry.split(" ")
                else:
                    raise Exception("Invalid submission order type")
                # print user_id + ", " + movie_id + ", " + date_id

                user_id = int(user_id)
                movie_id = int(movie_id)
                date_id = int(date_id)

                rating = np.dot(users[user_id].traits, movies[movie_id].traits)

                output_datafile.write(str(rating) + "\n")

                processed_data_count += 1
                if processed_data_count % 10000 == 0:
                    print "\rProcessing qual data...\tcompleted: " \
                        + str(processed_data_count) + " data entries",
                    sys.stdout.flush()

            print "\rProcessing qual data...\tDONE",
            print "                                                            "
            print "Processed a total of " + str(processed_data_count) \
                + " qual data entries"


if __name__ == '__main__':
    raise Exception("Processor_*.py files are not executable python scripts")
