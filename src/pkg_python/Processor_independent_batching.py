
import sys
from sets import Set

import numpy as np



def process(datatype, args):
    training_filename, training_indexfilename, qual_filename, output_filename = args

    # parse out data
    train_data = []
    with open(training_filename, "r") as training_datafile:
        with open(training_indexfilename, "r") as training_dataindexfile:
            data_indices = training_dataindexfile.readlines()

            parsed_data_count = 0
            for entry in training_datafile:
                index = int(data_indices[parsed_data_count].strip())

                # Each entry is (type_0_id, type_1_id, date_id, rating)
                # e.g. If the input data is movies_to_users, then type_0_id is
                # user_id and type_1_id is movie_id.
                user_id = ""
                movie_id = ""
                date_id = ""
                rating = ""

                # Parse data
                if datatype == "mu":
                    user_id, movie_id, date_id, rating = entry.split(" ")
                elif datatype == "um":
                    # movie_id, user_id, date_id, rating = entry.split(" ")
                    print "^This is wrong!"
                else:
                    raise Exception("Invalid submission order type")
                # print index
                # print user_id + ", " + movie_id + ", " + date_id,
                # print ", " + rating

                user_id, movie_id, date_id, rating = \
                    int(user_id), int(movie_id), int(date_id), int(rating)

                data_entry = (user_id, movie_id, date_id, rating)

                if index == 1:
                    if movie_id > len(train_data):
                        train_data += [[]]
                    else:
                        assert movie_id == len(train_data)
                        train_data[-1] += [(parsed_data_count, data_entry)]

                parsed_data_count += 1
                if parsed_data_count % 100000 == 0:
                    print "\rParsing data files...\tread: " \
                        + str(parsed_data_count) + " data entries",
                    sys.stdout.flush()

            print "\rParsing data files...\tDONE",
            print "                                                            "
            print "Parsed a total of " + str(parsed_data_count) \
                + " data entries"
            print "Train Data Group Count: " + str(len(train_data))

    # Batch data entries
    print "Batching..."
    batches = []
    new_batch = []
    included_users = Set()
    empty_group_count = 0
    search_start = len(train_data) - 1
    while empty_group_count != len(train_data):
        for i in range(len(train_data)):
            movie_group = train_data[i]
            if len(movie_group) == 0:
                continue

            if i == search_start:
                # print len(new_batch)
                # print new_batch
                batches += [new_batch]
                new_batch = []
                included_users.clear()

            checked_entry_count = 0
            for data_index, (user_id, movie_id, date_id, rating) in movie_group:
                if user_id not in included_users:
                    new_batch += [data_index]
                    if len(new_batch) == 1024:
                        # print len(new_batch)
                        # print new_batch
                        batches += [new_batch]
                        new_batch = []
                        included_users.clear()
                        search_start = i
                    else:
                        included_users.add(user_id)
                    break
                checked_entry_count += 1

            if checked_entry_count < len(movie_group):
                if len(movie_group) == 1:
                    empty_group_count += 1
                train_data[i] = movie_group[:checked_entry_count] \
                            + movie_group[checked_entry_count + 1:]

        if empty_group_count % 100000 == 0:
            print "\r" + str(empty_group_count) + " groups empty!",
            sys.stdout.flush()
    if len(new_batch) != 0:
        batches += [new_batch]
    print "\rDONE!                                                             "

    print "Total of: " + str(len(batches))

    print "Writing out data..."
    with open(output_filename, "w") as output_datafile:
        written_batch_count = 0
        for batch in batches:
            output_datafile.write(str(len(batch)) + "\n")
            for data_index in batch:
                output_datafile.write(str(data_index) + "\n")
            output_datafile.write("END\n")

            written_batch_count += 1
            if written_batch_count % 10000 == 0:
                print str(written_batch_count) + " data batches written out",
                sys.stdout.flush()
    print "\rDONE!                                                             "


if __name__ == '__main__':
    raise Exception("Processor_*.py files are not executable python scripts")
