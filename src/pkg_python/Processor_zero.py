
import sys

def process(datatype, args):
    training_filename, training_indexfilename, qual_filename, output_filename = args

    with open(training_filename, "r") as training_datafile:
        processed_data_count = 0
        for entry in training_datafile:
            user_num, movie_num, date_num, rating = entry.split(" ")
            # print user_num + ", " + movie_num + ", " + date_num + ", " + rating

            processed_data_count += 1
            if processed_data_count % 10000 == 0:
                print "\rProcessing train data\t\t\t...completed: " \
                    + str(processed_data_count) + " data entries",
                sys.stdout.flush()

        print "\rProcessing train data\t\t\t...DONE",
        print "                                                                "
        print "Processed a total of " + str(processed_data_count) \
            + " training data entries"
    print ""

    with open(qual_filename, "r") as qual_datafile:
        with open(output_filename, "w") as output_datafile:
            processed_data_count = 0
            for entry in qual_datafile:
                user_num, movie_num, date_num = entry.split(" ")
                # print user_num + ", " + movie_num + ", " + date_num

                output_datafile.write("2.500\n")

                processed_data_count += 1
                if processed_data_count % 10000 == 0:
                    print "\rProcessing qual data\t\t\t...completed: " \
                        + str(processed_data_count) + " data entries",
                    sys.stdout.flush()

            print "\rProcessing train data\t\t\t...DONE",
            print "                                                            "
            print "Processed a total of " + str(processed_data_count) \
                + " qual data entries"


if __name__ == '__main__':
    raise Exception("Processor_*.py files are not executable python scripts")
