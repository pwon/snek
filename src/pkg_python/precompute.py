# Calculates U(i, j), the number of users who have rated both item i and item
# j.

import sys
import numpy as np

movie_count = 17770
user_count = 458293

# Shrinkage parameter for similarity calculations.
alpha = 0.1
# Shrinkage parameter for the A-hat, b-hat entries.
beta = 500

# Filenames for the various things we want to precompute and save for later.
ufilename = "pairwisesupportsS.txt"
sfilename = "corrcoefS.txt"
afilename = "ahatS.txt"


def Similarity(i, j):
	'''
	Calculates the similarity of items i and j as #users-dim. vectors. i and j
	are expected to be NumPy arrays of shape (#users).
	'''
	# We can consider i and j to be a user_count-dim. vector of samples from
	# the random variables which are the true ratings of items i and j
	# respectively.
	return np.corrcoef(i, j)[0][1] # corrcoef returns matrix of corr. coeffs.


def process(datatype, training_filename, training_indexfilename):
	'''
	Using the datatype (which represents the data order) and a set of training
	files (training_filename, training_indexfilename), precomputes values such
	as |U(i, j)|, the size of the set of users who've rated both item i and j,
	the similarity scores s(i, j), the correlation coefficients between items
	i and j, and the \bar{A_{ij}}, for populating our later matrix.
	'''
	print "Initializing...\t\t\t",
	sys.stdout.flush()

	# A hashmap whose keys are movie IDs and whose values are a list of the
	# users who have rated that item, to allow for easy look up later
	# when we calculate |U(i, j)|.
	itemmap = dict()
	# Similarly, for fast lookup, we have a usermap.
	usermap = dict()
	# Finally, in lieu of just storing quadruples, we will instead store the
	# ratings in a hashtable with userid+movieid as the key. We use a hashmap
	# because the ratings matrix is sparse.
	ratings = dict()

	# Parse the training data into Python list of (uid, movid, dateid, rating)
	# quadruples.
	#train_data = []
	#test_data = []
	with open(training_filename, "r") as training_datafile:
		with open(training_indexfilename, "r") as training_dataindexfile:
			data_indices = training_dataindexfile.readlines()

			parsed_data_count = 0
			for entry in training_datafile:
				index = int(data_indices[parsed_data_count].strip())
				# Each entry is (type_0_id, type_1_id, date_id, rating)
				# e.g. If the input data is movies_to_users, then type_0_id is
				# user_id and type_1_id is movie_id.
				user_id = ""
				movie_id = ""
				date_id = ""
				rating = ""
				# Parse data
				if datatype == "mu":
					user_id, movie_id, date_id, rating = entry.split(" ")
				elif datatype == "um":
					movie_id, user_id, date_id, rating = entry.split(" ")
				else:
					raise Exception("Invalid submission order type")
				# print index
				# print user_id + ", " + movie_id + ", " + date_id,
				# print ", " + rating

				data_entry = (int(user_id), int(movie_id), int(date_id), int(rating))
				if index == 1:
					#train_data += [data_entry]
					# Populate our hashmaps with the training data.
					if data_entry[1] in itemmap:
						itemmap[data_entry[1]].append(data_entry[0])
					else:
						itemmap[data_entry[1]] = [data_entry[0]]
					if data_entry[0] in usermap:
						usermap[data_entry[0]].append(data_entry[1])
					else:
						usermap[data_entry[0]] = [data_entry[1]]
					ratings[user_id + "," + movie_id] = data_entry[3]
				#elif index != 5:
					#test_data += [data_entry]

				parsed_data_count += 1
				if parsed_data_count % 100000 == 0:
					print "\rParsing data files...\tread: " + str(parsed_data_count) + " data entries",
					sys.stdout.flush()

			print "\rParsing data files...\tDONE",
			print "                                                            "
			print "Parsed a total of " + str(parsed_data_count) + " data entries"
			#print "Train Data Count: " + str(len(train_data))
			#print "Test Data Count: " + str(len(test_data))
	# Now calculate the |U(i, j)| and A-bar[i][j] together. Note that the b
	# vectors are exactly the same as the A vectors in the end, since A_ij is
	# the exact same value as b_j when we want to predict a rating for item i.
	print "\rCalculating the |U(i,j)| and A-bar entries...",
	sys.stdout.flush()
	A = np.zeros((movie_count, movie_count))
	U = np.zeros((movie_count, movie_count))
	# Movie IDs (and user IDs) are 1-indexed, NOT 0-indexed.
	for i in range(1, movie_count + 1):
		for j in range(1, movie_count + 1):
			abar = 0
			count = 0
			if i not in itemmap:
				U[i - 1][j - 1] = 0
				A[i - 1][j - 1] = 0
				continue
			for uid in itemmap[i]:
				# Keys to usermap are ints, which uid is.
				if j in usermap[uid]:
					abar += ratings[str(uid) + "," + str(i)] * ratings[str(uid) + "," + str(j)]
					count += 1
			# Account for 1-indexing in array entries.
			U[i - 1][j - 1] = count
			# Divide by the support in the end.
			if count == 0:
				A[i - 1][j - 1] = 0
			else:
				A[i - 1][j - 1] = abar / float(count)
	print "\tDONE"
	# Save the U matrix to a csv. Currently not doing this to save memory.
	# print "\rSaving pairwise support matrix U to file: ", ufilename, " ...",
	# sys.stdout.flush()
	# np.savetxt(ufilename, U, delimiter = ",")
	# print "\tDONE"
	
	# Using the |U(i, j)| as part of our shrinkage regularization, we can then
	# calculate (regularized) similarity scores.
	print "\rCalculating the similarity scores...",
	sys.stdout.flush()
	S = np.zeros((movie_count, movie_count))
	for i in itemmap:
	# Make the vector corresponding to item i.
		i_vector = np.zeros(user_count)
		for userid in itemmap[i]:
			i_vector[userid - 1] = ratings[str(userid) + "," + str(i)] # 1-indexing
		for j in itemmap:
			j_vector = np.zeros(user_count)
			for userid in itemmap[j]:
				j_vector[userid - 1] = ratings[str(userid) + "," + str(j)] # 1-indexing
			sim = Similarity(i_vector, j_vector) # 1-indexing
			# Perform shrinkage using the hyperparameter alpha.
			S[i - 1][j - 1] = sim * (U[i - 1][j - 1] / float(U[i - 1][j - 1] + alpha)) # 1-indexing
	print "\tDONE"
	# Save the shrinkage-regularized similarities to a csv.
	print "\rSaving the similarity matrix S to file:", sfilename, "...",
	sys.stdout.flush()
	np.savetxt(sfilename, S, fmt = '%.10e')
	print "\tDONE"

	# Calculate the average value of the A-bar entries for shrinkage.
	print "\rCalculating the average values..."
	avg = 0
	avg_diag = 0
	for i in range(movie_count):
		for j in range(movie_count):
			if i == j:
				avg_diag += A[i][j]
			else:
				avg += A[i][j]
	avg = avg / float(movie_count ** 2 - movie_count)
	avg_diag = avg_diag / float(movie_count)
	print "Non-diagonal average is", avg
	print "Diagonal average is", avg_diag
	# TODO: IMPROVE USING VECTORIZATION?-------------------------------------

	# Calculate the A-hat values by applying shrinkage to the A-bar values
	# with the beta parameter.
	print "\rCalculating the A-hat entries...",
	sys.stdout.flush()
	for i in range(movie_count):
		for j in range(movie_count):
			baseline = 0
			if i == j:
				baseline = avg_diag
			else:
				baseline = avg
			A[i][j] = ((U[i][j] * A[i][j]) + (beta * baseline)) / float(U[i][j] + beta)
	print "\tDONE"
	# TODO: WRITE Abar matrix to file
	print "\rSaving the A-bar matrix to file:", afilename, "...",
	sys.stdout.flush()
	np.savetxt(afilename, A, fmt = '%.10e')
	print "\tDONE"

def main():
	datatype = 'mu'
	training_filename = '../../data/movies_to_users/f10000.dta'
	training_indexfilename = '../../data/movies_to_users/f10000.idx'
	process(datatype, training_filename, training_indexfilename)

if __name__ == '__main__':
	main()

