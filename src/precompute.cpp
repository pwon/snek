// Precomputes the similarity matrix with shrinkage.

#include "common.hpp"
#include "similarity.hpp"
#include "data.hpp"
#include <string>

#define NITEMS 17770
#define NUSERS 458293

int index(int i, int j)
{
	return (NITEMS * i) + j;
}

int main()
{
	Data::init();
	// Data::cache(); // Run once to cache data as binary files.
	Data::load();
	// Maybe switch over to Eigen eventually?
	std::vector<float> S (NITEMS * NITEMS, 0.0);
	// std::vector<int> U (NITEMS * NITEMS, 0.0);
	std::unordered_map<int, std::vector<int>> itemmap;
	// std::unordered_map<int, std::vector<int>> usermap;
	std::unordered_map<std::string, int> ratings;

	std::cout << "Processing training data..." << std::endl;
	for (int i = 0; i < Data::train.size(); i++)
	{
		Data::Entry entry = Data::train[i];
		if (itemmap.find(entry.movie_id) != itemmap.end())
		{
			itemmap[entry.movie_id].push_back(entry.user_id);
		}
		else
		{
			std::vector<int> t (1, entry.user_id);
			itemmap[entry.movie_id] = t;
		}
		// if (usermap.find(entry.user_id) != usermap.end())
		// {
		// 	usermap[entry.user_id].push_back(entry.movie_id);
		// }
		// else
		// {
		// 	std::vector<int> t (1, entry.movie_id);
		// 	usermap[entry.user_id] = t;
		// }
		ratings[std::to_string(entry.user_id) + "," + std::to_string(entry.movie_id)] = entry.rating;
	}
	std::cout << "DONE!" << std::endl;

	// std::cout << "Calculating the pairwise supports |U(i, j)|..." << std::endl;
	// // Calculate the pairwise supports U(i, j).
	// for (int i = 1; i < NITEMS + 1; i++)
	// {
	// 	std::vector<int> users_rating_i = itemmap[i];
	// 	std::sort(users_rating_i.begin(), users_rating_i.end());
	// 	for (int j = i; j < NITEMS + 1; j++)
	// 	{
	// 		// std::cout << "Calculating U(" << i << ", " << j << ")..." << std::endl;
	// 		int count = 0;
	// 		std::vector<int> users_rating_j = itemmap[j];
	// 		// Sort in-place.
	// 		std::sort(users_rating_j.begin(), users_rating_j.end());
	// 		// For each elements that we find in common in users_rating_i and
	// 		// users_rating_j, increment the count.
	// 		// int k = 0; // Pointer into users_rating_i
	// 		// int l = 0; // Pointer into users_rating_j
	// 		// while (k < users_rating_i.size() and l < users_rating_j.size())
	// 		// {
	// 		// 	if (users_rating_i[k] == users_rating_j[l])
	// 		// 	{
	// 		// 		count++;
	// 		// 		k++;
	// 		// 		l++;
	// 		// 	}
	// 		// 	else if (users_rating_i[k] > users_rating_j[l])
	// 		// 	{
	// 		// 		l++;
	// 		// 	}
	// 		// 	else
	// 		// 	{
	// 		// 		k++;
	// 		// 	}
	// 		// }
	// 		std::vector<int> v(users_rating_i.size() + users_rating_j.size());
	// 		std::vector<int>::iterator it = std::set_intersection(users_rating_i.begin(), users_rating_i.end(), users_rating_j.begin(), users_rating_j.end(), v.begin());
	// 		v.resize(it - v.begin());
	// 		count = v.size();
	// 		U[index(i - 1, j - 1)] = count;
	// 		U[index(j - 1, i - 1)] = count;
	// 		// std::cout << "DONE!" << std::endl;
	// 	}
	// 	if (i % 10 == 0)
	// 	{
	// 		std::cout << "Finished calculating row " << i << "!" << std::endl;
	// 	}
	// }
	// std::cout << "DONE!\n" << std::endl;

	// std::cout << "Writing U to file...\n" << std::endl;
	// std::ofstream binary_U_data_file;
	// binary_U_data_file.open("../cache/movies_to_users/corrcoef.bch", std::ios::out | std::ios::binary);
	// binary_U_data_file.write((char *) U.data(), NITEMS * NITEMS * sizeof(int));
	// binary_U_data_file.close();
	// std::cout << "DONE!\n" << std::endl;

	std::cout << "Calculating the similarity matrix S..." << std::endl;
	// Get the raw (non-shrinked) similarities.
	int ecount = 0;
	std::unordered_map<int, std::vector<int>>::iterator p, q;
	for (p = itemmap.begin(); p != itemmap.end(); p++)
	{
		std::vector<std::pair<int, float>> ivec1; // User ID, rating pair
		// int itemkey = p->first;
		// std::vector<int> itemval = p->second;
		// Value of keys are 1-indexed item ids.
		// Value of values are std::vector<int> of 1-indexed user IDs.
		// for (int i = 0; i < p->second.size(); i++)
		// {
		// 	ivec1[p->second[i] - 1] = (float) ratings[std::to_string(p->second[i]) + "," + std::to_string(p->first)];
		// }
		for (int i = 0; i < p->second.size(); i++)
		{
			int rating_i = ratings[std::to_string(p->second[i]) + "," + std::to_string(p->first)];
			ivec1.push_back(std::pair<int, float>(p->second[i], rating_i));
		}
		for (q = itemmap.begin(); q != itemmap.end(); q++)
		{
			if (p->first > q->first)
			{
				continue;
			}
			// std::vector<float> ivec2 (NUSERS, 0.0);
			// for (int j = 0; j < q->second.size(); j++)
			// {
			// 	ivec2[q->second[j] - 1] = (float) ratings[std::to_string(q->second[j]) + "," + std::to_string(q->first)];
			// }
			std::vector<std::pair<int, float>> ivec2;
			for (int j = 0; j < q->second.size(); j++)
			{
				int rating_j = ratings[std::to_string(q->second[j]) + "," + std::to_string(q->first)];
				ivec2.push_back(std::pair<int, float>(q->second[j], rating_j));
			}
			// Can add other similarity functions.
			float sim = corrCoef2(ivec1, ivec2);
			S[index(p->first - 1, q->first - 1)] = sim;
			S[index(q->first - 1, p->first - 1)] = sim;
			ecount++;
			if (ecount % 10000 == 0)
			{
				std::cout << "Calculated " << ecount << " entries!" << std::endl;
			}
		}
	}
	std::cout << "DONE!" << std::endl;
	
	// Write matrix out to a binary file.
	std::cout << "Writing S to file..." << std::endl;
	std::ofstream binary_out_data_file;
	binary_out_data_file.open("../cache/movies_to_users/corrcoef.bch", std::ios::out | std::ios::binary);
	binary_out_data_file.write((char *) S.data(), NITEMS * NITEMS * sizeof(float));
	binary_out_data_file.close();
	std::cout << "DONE!\n" << std::endl;

	return 0;
}