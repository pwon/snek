// Implementation of similarity functions.

#include "similarity.hpp"

float corrCoef(std::vector<float> &item1, std::vector<float> &item2)
{
	float sum1, sqsum1, dot, sum2, sqsum2 = 0;
	int n = item1.size();
	for (int i = 0; i < n; i++)\
	{
		sum1 += item1[i];
		sqsum1 += item1[i] * item1[i];
		dot += item1[i] * item2[i];
		sum2 += item2[i];
		sqsum2 += item2[i] * item2[i];
	}
	return ((n * dot) - (sum1 * sum2)) / sqrt(((n * sqsum1) - (sum1 * sum1)) * ((n * sqsum2) - (sum2 * sum2)));
}

float corrCoef2(std::vector<std::pair<int, float>> &item1, std::vector<std::pair<int, float>> &item2)
{
	// Find the intersection of the indices and add those terms to the dot
	// product. Assumes that item1 and item2 are sorted in order of increasing
	// index (the first element in the pair).
	std::vector<float> vec1;
	std::vector<float> vec2;
	int i = 0;
	int j = 0;
	int dot = 0;
	while (i < item1.size() and j < item2.size())
	{
		if (item1[i].first < item2[j].first)
		{
			i++;
		}
		else if (item1[i].first > item2[j].first)
		{
			j++;
		}
		else
		{
			// dot += item1[i].second * item2[j].second;
			vec1.push_back(item1[i].second);
			vec2.push_back(item2[j].second);
			i++;
			j++;
		}
	}
	return corrCoef(vec1, vec2);
}