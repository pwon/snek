// Header for the similarity functions.
#pragma once

#include <math.h>
#include <utility>
#include "common.hpp"

float corrCoef(std::vector<float> &item1, std::vector<float> &item2);

float corrCoef2(std::vector<std::pair<int, float>> &item1, std::vector<std::pair<int, float>> &item2);

//float setCorr(std::vector<float> item1, std::vector<float> item2);